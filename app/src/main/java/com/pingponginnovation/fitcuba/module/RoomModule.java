package com.pingponginnovation.fitcuba.module;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.pingponginnovation.fitcuba.BuildConfig;
import com.pingponginnovation.fitcuba.persistence.database.AppDatabase;
import com.pingponginnovation.fitcuba.persistence.database.backup.surveys.BackupSurveyDao;
import com.pingponginnovation.fitcuba.persistence.database.backup.visits.BackupBeaconDao;
import com.pingponginnovation.fitcuba.persistence.database.events.EventDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = ContextModule.class)
public class RoomModule {

  private static final String APP_DATABASE_NAME = BuildConfig.APPLICATION_ID + ".AppDatabase";

  // TODO Remove .fallbackToDestructiveMigration() call when app goes to production and ensure migration is in place for all DB chnages

  @Provides
  @Singleton
  public AppDatabase provideAppDatabase(Context applicationContext) {
    return Room.databaseBuilder(applicationContext, AppDatabase.class, APP_DATABASE_NAME)
        .fallbackToDestructiveMigration()
        .build();
  }

  @Provides
  public EventDao provideEventDao(AppDatabase appDatabase) {
    return appDatabase.eventDao();
  }

  @Provides
  public BackupSurveyDao provideSurveyDao(AppDatabase appDatabase) {
    return appDatabase.backupSurveyDao();
  }

  @Provides
  public BackupBeaconDao provideBeaconDao(AppDatabase appDatabase) {
    return appDatabase.backupBeaconDao();
  }
}
