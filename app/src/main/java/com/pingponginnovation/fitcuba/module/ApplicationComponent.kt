package com.pingponginnovation.fitcuba.module

import com.pingponginnovation.fitcuba.ui.core.CoreActivity
import com.pingponginnovation.fitcuba.ui.core.home.HomeFragment
import com.pingponginnovation.fitcuba.ui.detailevent.DetailEventFragment
import com.pingponginnovation.fitcuba.ui.detailevent.innerdetailevent.InnerDetailFragment
import com.pingponginnovation.fitcuba.ui.detailevent.innerdetailevent.survey.SingleSurveyFragment
import com.pingponginnovation.fitcuba.ui.login.LoginActivity
import com.pingponginnovation.fitcuba.ui.login.SessionFragment
import com.pingponginnovation.fitcuba.ui.login.register.RegisterFragment
import com.pingponginnovation.fitcuba.ui.login.signin.SignInFragment
import com.pingponginnovation.fitcuba.ui.slider.SingleSliderFragment
import com.pingponginnovation.fitcuba.ui.slider.SliderWelcomeActivity
import com.pingponginnovation.fitcuba.ui.splash.SplashScreenActivity
import com.pingponginnovation.fitcuba.ui.util.LoadingDialogFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ContextModule::class, PreferencesModule::class, NetworkModule::class, RoomModule::class])
interface ApplicationComponent {

    //Injects

    fun inject(coreActivity: CoreActivity)

    fun inject(splashScreenActivity: SplashScreenActivity)

    fun inject(loginActivity: LoginActivity)

    fun inject(sliderWelcomeActivity: SliderWelcomeActivity)

    fun inject(loadingDialogFragment: LoadingDialogFragment)

    fun inject(detailEventFragment: DetailEventFragment)

    fun inject(innerDetailFragment: InnerDetailFragment)

    fun inject(homeFragment: HomeFragment)

    fun inject(sessionFragment: SessionFragment)

    fun inject(signInFragment: SignInFragment)

    fun inject(singleSliderFragment: SingleSliderFragment)

    fun inject(registerFragment: RegisterFragment)

    fun inject(singleSurveyFragment: SingleSurveyFragment)
}