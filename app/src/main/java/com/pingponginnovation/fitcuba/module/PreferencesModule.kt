package com.pingponginnovation.fitcuba.module

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import java.lang.UnsupportedOperationException

@Module(includes = [ContextModule::class])
class PreferencesModule {

    fun PreferencesModule() {
        throw UnsupportedOperationException()
    }

    @Module(includes = [ContextModule::class])
    companion object {
        val PREFERENCES_NAME = "com.pingponginnovation.fitcuba#main-preferences"

        @JvmStatic
        @Provides
        fun providesSharedPreferences(context : Context) : SharedPreferences {
            return context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
        }
    }
}