package com.pingponginnovation.fitcuba.module.glide

import android.content.Context
import com.bumptech.glide.Glide
import com.bumptech.glide.Registry
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.module.AppGlideModule
import okhttp3.OkHttpClient
import timber.log.Timber

import javax.net.ssl.SSLContext
import javax.net.ssl.X509TrustManager
import java.io.InputStream
import java.security.KeyManagementException
import java.security.NoSuchAlgorithmException
import java.security.cert.X509Certificate

@GlideModule
class StandardAppGlideModule : AppGlideModule() {

    override fun registerComponents(context: Context, glide: Glide, registry: Registry) {
        try {
            val okHttpClient = unsafeOkHttpClient()
            registry.replace(
                GlideUrl::class.java,
                InputStream::class.java,
                OkHttpUrlLoader.Factory(okHttpClient)
            )
        } catch (e: NoSuchAlgorithmException) {
            Timber.e("Error creating http client " + e.message)
        } catch (e: KeyManagementException) {
            Timber.e("Error creating http client " + e.message)
        }

    }

    @Throws(NoSuchAlgorithmException::class, KeyManagementException::class)
    private fun unsafeOkHttpClient(): OkHttpClient {
        val unsafeTrustManager = createUnsafeTrustManager()
        val sslContext = SSLContext.getInstance(SSL)
        sslContext.init(null, arrayOf(unsafeTrustManager), null)
        return OkHttpClient.Builder().sslSocketFactory(sslContext.socketFactory, unsafeTrustManager)
            .build()
    }

    private fun createUnsafeTrustManager(): X509TrustManager {
        return object : X509TrustManager {
            override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {
                //no op
            }

            override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {
                //no op
            }

            override fun getAcceptedIssuers(): Array<X509Certificate?> {
                return arrayOfNulls(0)
            }
        }
    }

    companion object {
        private const val SSL = "SSL"
    }
}
