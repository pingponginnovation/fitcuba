package com.pingponginnovation.fitcuba.ui.util.forms

interface FormCallback {

    fun formError(error: FormError)

}