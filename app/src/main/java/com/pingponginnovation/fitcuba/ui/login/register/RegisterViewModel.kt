package com.pingponginnovation.fitcuba.ui.login.register

import android.arch.lifecycle.ViewModel
import com.pingponginnovation.fitcuba.data.login.LoginRepository
import com.pingponginnovation.fitcuba.ui.login.UiLoginResult
import com.pingponginnovation.fitcuba.ui.login.signin.LoginUiMapper
import com.pingponginnovation.fitcuba.ui.util.NoOpDisposable
import com.pingponginnovation.fitcuba.ui.util.forms.*
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class RegisterViewModel @Inject
internal constructor(
    private val loginRepository: LoginRepository,
    private val uiMapper: LoginUiMapper,
    private val formHelper: FormHelper
) : ViewModel(), FormCallback {

    init {
        initFormHelper()
    }

    private val resultSubject = PublishSubject.create<UiLoginResult>()
    private var disposable: Disposable = NoOpDisposable()

    private val fieldErrorSubject = PublishSubject.create<FormError>()

    private fun initFormHelper() {
        val fieldList = Arrays.asList(
            FormField.Email,
            FormField.Password,
            FormField.Country,
            FormField.Birthday
        )
        formHelper.initialize(this, fieldList)
    }

    fun getCountries(): Single<List<String>> {
        return loginRepository.getCountries()
    }

    fun getSignUpResult(): Observable<UiLoginResult> {
        return resultSubject
    }

    fun validateField(input: FormInput): Boolean {
        return formHelper.validateField(input)
    }

    fun errorUpdates(): Observable<FormError> {
        return fieldErrorSubject
    }

    fun submitNewUser(email: String, password: String, birthday: String, idCountry: Int) {
        disposable.dispose()
        disposable = loginRepository.submitNewUser(email, password, birthday, idCountry)
            .map(uiMapper::toUiSignInResult)
            .doOnError { error ->
                Timber.w(
                    "Error submitting login information: %s",
                    error.message
                )
            }
            .subscribe(resultSubject::onNext)
    }

    override fun formError(error: FormError) {
        fieldErrorSubject.onNext(error)
    }
}