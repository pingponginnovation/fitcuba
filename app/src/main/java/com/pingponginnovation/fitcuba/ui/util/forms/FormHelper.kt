package com.pingponginnovation.fitcuba.ui.util.forms

import android.support.annotation.NonNull
import com.pingponginnovation.fitcuba.R
import com.pingponginnovation.fitcuba.ui.util.FieldValidationUtil
import java.util.*
import javax.inject.Inject

class FormHelper @Inject
internal constructor(private val fieldValidationUtil: FieldValidationUtil) {

    private val FieldLenghtMaxUser = 4
    private val FieldLenghtMaxPassword = 8

    lateinit var callback: FormCallback

    private val fieldValidationMap: EnumMap<FormField, Boolean> = EnumMap(FormField::class.java)
    private val NonRequiredFields = emptyList<FormField>()


    fun initialize(@NonNull callback: FormCallback, fieldList: List<FormField>) {
        this.callback = callback

        for (field in fieldList) {
            fieldValidationMap[field] = false

            // init as true, since these fields are not required
            if (NonRequiredFields.contains(field)) {
                fieldValidationMap[field] = true
            }
        }
    }

    private fun assertInitialized() {
        if (callback == null) {
            throw IllegalStateException("You've attempted to use FormHelper without first calling formHelper.initialize()")
        }
    }

    fun validateField(input: FormInput): Boolean {
        assertInitialized()

        var validated = validateRequired(input)
        if (validated) {
            validated = validateLength(input)
        }
        if (validated) {
            validated = validateSpecifics(input)
        }

        setFieldValidated(input, validated)
        return validated
    }

    private fun validateRequired(input: FormInput): Boolean {
        when (input.field) {
            FormField.User,
            FormField.Birthday,
            FormField.Country,
            FormField.Email,
            FormField.Password,
            FormField.PasswordConfirm
            -> if (fieldValidationUtil.isEmpty(input.value) || input.value == "0") {
                val field = when(input.field) {
                    FormField.User -> "correo"
                    FormField.Birthday -> "año de nacimiento"
                    FormField.Country -> "ciudad"
                    FormField.Email -> "email"
                    FormField.Password -> "contraseña"
                    FormField.PasswordConfirm -> "confirmar contraseña"
                }
                callback.formError(FormError(input.field, R.string.field_is_required, field))
                return false
            }
        }
        return true
    }

    private fun validateLength(input: FormInput): Boolean {
        when (input.field) {
            FormField.User -> if (fieldValidationUtil.isShorterThan(
                    input.value,
                    FieldLenghtMaxUser
                )
            ) {
                callback.formError(FormError(input.field, R.string.field_requires_fewer_chars, "usuario"))
                return false
            }
            FormField.Password,
            FormField.PasswordConfirm -> if (fieldValidationUtil.isShorterThan(
                    input.value,
                    FieldLenghtMaxPassword
                )
            ) {
                callback.formError(FormError(input.field, R.string.field_requires_fewer_chars, "contraseña"))
                return false
            }
        }
        return true
    }

    private fun validateSpecifics(input: FormInput): Boolean {
        when (input.field) {
            FormField.Email -> if (!fieldValidationUtil.validateEmail(input.value)) {
                callback.formError(FormError(input.field, R.string.field_error_invalid_email))
                return false
            }
            FormField.Password -> if (!fieldValidationUtil.validatePasswordFormat(input.value)) {
                callback.formError(
                    FormError(
                        input.field,
                        R.string.field_error_password_incorrect_format
                    )
                )
                return false
            }
            FormField.PasswordConfirm -> if (input.value != input.compareValue) {
                callback.formError(
                    FormError(
                        input.field,
                        R.string.field_error_non_matching_password
                    )
                )
                return false
            }
        }
        return true
    }

    private fun setFieldValidated(input: FormInput, validated: Boolean) {
        fieldValidationMap[input.field] = validated
    }
}