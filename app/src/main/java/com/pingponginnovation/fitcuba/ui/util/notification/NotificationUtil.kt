package com.pingponginnovation.fitcuba.ui.util.notification

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.PowerManager
import android.support.v4.app.NotificationCompat
import com.pingponginnovation.fitcuba.R
import com.pingponginnovation.fitcuba.data.core.HomeRepository
import com.pingponginnovation.fitcuba.persistence.database.events.EventStore
import com.pingponginnovation.fitcuba.ui.base.BaseActivity
import com.pingponginnovation.fitcuba.ui.core.CoreActivity
import com.pingponginnovation.fitcuba.ui.detailevent.UiNotification
import com.pingponginnovation.fitcuba.ui.util.ToastUtil
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NotificationUtil @Inject
internal constructor(private val context: Context, private val toastUtil: ToastUtil,
                     private val database: EventStore, private val homeRepository: HomeRepository
) {

    private val TimeScreenOn:Long = 3000
    private val Tag = "MH24_SCREENLOCK"
    private val Beacon = "BEACON"
    private val Message = "Sin información"
    private var readPermanently = true
    private lateinit var  messageBeacon: String
    private var  idPlace: Int = 0
    private lateinit var  baseActivity: BaseActivity


    @SuppressLint("InvalidWakeLockTag")
    fun turnOnScreen(baseActivity: BaseActivity) {
        val powerManager = baseActivity.getSystemService(Context.POWER_SERVICE) as PowerManager
        val wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK or PowerManager.ACQUIRE_CAUSES_WAKEUP or PowerManager.ON_AFTER_RELEASE, Tag)
        wakeLock.acquire(TimeScreenOn)
        wakeLock.release()
    }

    fun shouldCreateNotification(create: Boolean, nameDeviceBluethoot: String, baseActivity: BaseActivity){
        this.baseActivity = baseActivity
        if(create){
            database.getDataBeacon(nameDeviceBluethoot)
                .subscribe{ result -> setInformationToNotification(result)}
        }
    }

    fun handleBehaviorNotification(readPermanently: Boolean){
        if(readPermanently){
            toastUtil.showToast(context, R.string.notification_read_continues_activate)
        } else {
            toastUtil.showToast(context,R.string.notification_read_continues_deactivate)
        }
        this.readPermanently = readPermanently
    }

    private fun setInformationToNotification(beacon: UiNotification) {
        //This way could be implemented
        if (beacon.body.isNotEmpty()) {
            this.messageBeacon = beacon.body
            this.idPlace = beacon.idPlace
        } else {
            this.messageBeacon = "Sin información"
        }
        if (this.messageBeacon != Message){
            registerVisitOnServer(beacon.idBeacon)
            showNotificationBeacon(beacon)
        }
    }

    private fun registerVisitOnServer(idBeacon: Int) {
        homeRepository.registerVisitOnServer(idBeacon)
    }

    private fun showNotificationBeacon(beacon: UiNotification) {
        //This way could be implemented
        val intent = Intent(context, CoreActivity::class.java)
        intent.putExtra(Beacon, beacon)
        val pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        val sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        createNotificationBeacon(pendingIntent, sound)
    }

    private fun createNotificationBeacon(pendingIntent: PendingIntent, sound: Uri) {
        val notification = NotificationCompat.Builder(context)
            .setSmallIcon(R.drawable.ic_notification)
            .setContentTitle("Fitcuba")
            .setContentText(messageBeacon)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
        turnOnScreen(baseActivity)
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            notification.setChannelId("MY_CHANNEL")
            val mChanel = NotificationChannel("MY_CHANNEL", "Fitcuba", NotificationManager.IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(mChanel)
            if(readPermanently) {
                notificationManager.notify(1, notification.build())
            } else {
                notification.setSound(sound)
                notificationManager.notify(createRandomNumber(), notification.build())
            }

        } else {
            if(readPermanently) {
                notificationManager.notify(1, notification.build())
            } else {
                notification.setSound(sound)
                notificationManager.notify(createRandomNumber(), notification.build())
            }
        }
    }

    private fun createRandomNumber(): Int {
        return Random().nextInt(100)
    }

}