package com.pingponginnovation.fitcuba.ui.detailevent

interface DetailEventCallback {

    fun goToInnerDetail()

}