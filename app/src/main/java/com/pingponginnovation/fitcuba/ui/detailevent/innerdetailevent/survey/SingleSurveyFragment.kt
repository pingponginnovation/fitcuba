package com.pingponginnovation.fitcuba.ui.detailevent.innerdetailevent.survey

import android.os.Bundle
import android.view.View
import com.metova.slim.annotation.Callback
import com.metova.slim.annotation.Layout
import com.pingponginnovation.fitcuba.R
import com.pingponginnovation.fitcuba.module.ApplicationComponentHolder
import com.pingponginnovation.fitcuba.ui.base.ViewModelFragment
import com.pingponginnovation.fitcuba.ui.detailevent.DetailEventViewModel
import com.pingponginnovation.fitcuba.ui.detailevent.innerdetailevent.UiSurvey
import com.pingponginnovation.fitcuba.ui.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_single_survey.*

@Layout(R.layout.fragment_single_survey)
class SingleSurveyFragment : ViewModelFragment<DetailEventViewModel>() {

    companion object {

        private const val Survey = "SURVEY"
        private const val Callback = "CALLBACK"
        private var callback: CallbackSurvey? = null

        fun newInstance(uiSurvey: UiSurvey, callback: CallbackSurvey): SingleSurveyFragment{
            val fragment = SingleSurveyFragment()
            val args = Bundle()
            args.putSerializable(Survey, uiSurvey)
            fragment.arguments = args
            this.callback = callback
            return fragment
        }
    }

    @Callback
    lateinit var navigation: Navigation

    override fun viewModelClass() = DetailEventViewModel::class.java

    override fun inject() = ApplicationComponentHolder.INSTANCE.getComponent().inject(this)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setInformation()
    }

    private fun setInformation() {
        val uiSurvey = arguments?.getSerializable(Survey) as UiSurvey
        uiSurvey.let {
            bk_survey.setBackgroundColor(resources.getColor(it.colorBackground))
            txt_question.text = it.question
            im_like.setOnClickListener { callback!!.answer(1, uiSurvey.idSurvey) }
            im_dislike.setOnClickListener { callback!!.answer(0, uiSurvey.idSurvey) }
        }
    }
}
