package com.pingponginnovation.fitcuba.ui.overlay

import android.os.Bundle
import android.view.View
import com.pingponginnovation.fitcuba.ui.base.ViewModelActivity
import io.reactivex.android.schedulers.AndroidSchedulers

abstract class BaseOverlayActivity<VM : BaseOverlayViewModel> : ViewModelActivity<VM>(){

    private val ExtraOverlayType = "overlay_type"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadBackgroundImage()
    }

    protected fun getTypeFromIntent(): Enum<*> {
        if (intent.extras != null && intent.extras!!.containsKey(ExtraOverlayType)) {
            return intent.extras!!.get(ExtraOverlayType) as Enum<*>
        }

        throw UnsupportedOperationException("Overlay activity requires a type specification")
    }

    private fun loadBackgroundImage() {
        addSubscription(viewModel().getBlurredBackground()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { backgroundDrawable ->
                getRootViewForBlurredBackground().background = backgroundDrawable
            })
    }

    protected abstract fun getRootViewForBlurredBackground(): View
}