package com.pingponginnovation.fitcuba.ui.util.glide

import android.content.Context
import android.graphics.drawable.Drawable
import android.widget.ImageView

import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.request.target.ViewTarget
import com.bumptech.glide.signature.ObjectKey
import com.pingponginnovation.fitcuba.module.glide.GlideApp
import com.pingponginnovation.fitcuba.module.glide.GlideRequests

import javax.inject.Inject
import java.io.File

class GlideWrapper @Inject
internal constructor() {

    fun with(context: Context): GlideRequests {
        return GlideApp.with(context)
    }

    fun loadDrawableWithSignature(context: Context, file: File): RequestBuilder<Drawable> {
        return GlideApp.with(context)
            .load(file)
            .signature(ObjectKey(file.lastModified()))
    }

    fun loadDrawableFromUrl(
        context: Context,
        url: String,
        imageView: ImageView
    ): ViewTarget<ImageView, Drawable> {
        return GlideApp.with(context)
            .load(url)
            .into(imageView)
    }
}
