package com.pingponginnovation.fitcuba.ui.util.notification.firebase

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.support.v4.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.pingponginnovation.fitcuba.R
import com.pingponginnovation.fitcuba.ui.core.CoreActivity
import com.pingponginnovation.fitcuba.ui.detailevent.UiNotification
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ReceiverNotification@Inject
internal constructor() : FirebaseMessagingService() {

    private val Beacon = "BEACON"

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        super.onMessageReceived(remoteMessage)
        //Cuando solo es notificacion es getNotification
        if (remoteMessage!!.notification != null) {
            val title = remoteMessage.notification!!.title!!
            val body = remoteMessage.notification!!.body!!
            //Para cuando se administra totalmente la notificacion es getData
            val city = remoteMessage.data["cities_id"]?.toInt()
            val place = remoteMessage.data["element_id"]?.toInt()
            val isEvent = remoteMessage.data["is_event"]?.toInt()
            val uiNotificationUtil = UiNotification(0, title, body, place ?: 0, city ?: 0, isEvent ?: 0)

            showNotificationBeacon(uiNotificationUtil)
        }
    }

    private fun showNotificationBeacon(beacon: UiNotification) {
        //This way could be implemented
        val intent = Intent(this@ReceiverNotification, CoreActivity::class.java)
        intent.putExtra(Beacon, beacon)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        val sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        createNotificationBeacon(pendingIntent, sound, beacon)
    }

    private fun createNotificationBeacon(pendingIntent: PendingIntent, sound: Uri, beacon: UiNotification) {
        val notification = NotificationCompat.Builder(this)
            .setSmallIcon(R.drawable.ic_notification)
            .setContentTitle(beacon.title)
            .setContentText(beacon.body)
            .setContentIntent(pendingIntent)
            .setSound(sound)
            .setAutoCancel(true)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            notification.setChannelId("MY_CHANNEL")
            val mChanel = NotificationChannel("MY_CHANNEL", "Fitcuba", NotificationManager.IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(mChanel)
            notificationManager.notify(1, notification.build())
        } else {
            notificationManager.notify(1, notification.build())
        }
    }

    companion object {
        private val TAG = "RECIBIR"
    }
}