package com.pingponginnovation.fitcuba.ui.base

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.pingponginnovation.fitcuba.R
import com.pingponginnovation.fitcuba.ui.core.CoreActivity
import com.pingponginnovation.fitcuba.ui.core.home.HomeFragment
import com.pingponginnovation.fitcuba.ui.core.home.UiHome
import com.pingponginnovation.fitcuba.ui.detailevent.DetailEventFragment
import com.pingponginnovation.fitcuba.ui.detailevent.innerdetailevent.InnerDetailFragment
import com.pingponginnovation.fitcuba.ui.detailevent.innerdetailevent.UiInnerDetailEvent
import com.pingponginnovation.fitcuba.ui.login.LoginActivity
import com.pingponginnovation.fitcuba.ui.login.SessionFragment
import com.pingponginnovation.fitcuba.ui.login.register.RegisterFragment
import com.pingponginnovation.fitcuba.ui.login.signin.SignInFragment
import com.pingponginnovation.fitcuba.ui.navigation.Navigation
import com.pingponginnovation.fitcuba.ui.navigation.UiAction
import com.pingponginnovation.fitcuba.ui.slider.SliderWelcomeActivity
import com.pingponginnovation.fitcuba.ui.util.SnackbarUtil
import com.pingponginnovation.fitcuba.ui.util.ToastUtil
import timber.log.Timber
import javax.inject.Inject

abstract class NavigationActivity: BaseActivity(), Navigation {

    @Inject protected lateinit var snackbarUtil: SnackbarUtil
    @Inject protected lateinit var toastUtil: ToastUtil

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        inject()
    }

    protected abstract fun inject()

    override fun navigateBack() {
        onBackPressed()
    }

    override fun finishActivity() {
        finish()
    }

    override fun navigateToAction(uiAction: UiAction) {
        when(uiAction) {
            UiAction.Core -> launchActivityInClearTask(CoreActivity::class.java)
            UiAction.Home -> {
                verifyActivity(CoreActivity::class.java)
                launchFragment(HomeFragment())
            }
            UiAction.InnerDetail -> {
                verifyActivity(CoreActivity::class.java)
                launchFragment(InnerDetailFragment())
            }
            UiAction.Login -> launchActivityInClearTask(LoginActivity::class.java)
            UiAction.Session -> {
                verifyActivity(LoginActivity::class.java)
                launchFragment(SessionFragment())
            }
            UiAction.SignIn -> {
                verifyActivity(LoginActivity::class.java)
                launchFragment(SignInFragment())
            }
            UiAction.Slide -> launchActivity(SliderWelcomeActivity::class.java)
            UiAction.Register -> {
                verifyActivity(LoginActivity::class.java)
                launchFragment(RegisterFragment())
            }
            else -> {
                showGenericSnackbar(R.string.launch_action_not_supported)
            }
        }
    }

    override fun <T> navigateToAction(action: UiAction, argument: T, nameArgument: String) {
        when (action) {
            UiAction.DetailEvent -> {
                if (argument is UiHome) {
                    verifyActivity(CoreActivity::class.java)
                    launchFragment(DetailEventFragment.newInstance(argument as UiHome))
                } else {
                    verifyActivity(CoreActivity::class.java)
                    launchFragment(DetailEventFragment.newInstance(argument as UiInnerDetailEvent))
                }
            }
            else -> {
                showGenericSnackbar(R.string.launch_action_not_supported)
            }
        }
    }


    private fun showGenericSnackbar(@StringRes message: Int) {
        snackbarUtil.showSnackbar(findViewById(android.R.id.content), message)
    }

    private fun launchActivity(activity: Class<out AppCompatActivity>) {
        val intent = Intent(this, activity)
        intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
        launchActivityIntent(intent)
    }

    private fun launchActivityIntentSingleTop(intent: Intent) {
        intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
        launchActivityIntent(intent)
    }

    private fun launchActivityInClearTask(activity: Class<out AppCompatActivity>) {
        val intent = Intent(this, activity)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        launchActivityIntent(intent)
    }

    private fun launchActivityIntent(intent: Intent) {
        startActivity(intent)
    }

    private fun launchFragment(fragment: Fragment) {
        launchFragment(fragment, true)
    }

    private fun launchFragment(fragment: Fragment, addToBackStack: Boolean) {
        val fragmentManager = supportFragmentManager
        val isFirstFragment = fragmentManager.fragments.isEmpty()
        val ft = fragmentManager.beginTransaction()
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        ft.replace(android.R.id.content, fragment, fragment.tag)
        if (addToBackStack && !isFirstFragment) {
            ft.addToBackStack(fragment.javaClass.simpleName)
        }

        ft.commit()
    }

    private fun verifyActivity(activityClass: Class<out AppCompatActivity>) {
        if( !activityClass.isInstance(this)){
            throw IllegalStateException("Launching fragment is not avaliable from this activity")
        }
    }
}