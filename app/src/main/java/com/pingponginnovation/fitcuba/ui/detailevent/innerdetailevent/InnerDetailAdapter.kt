package com.pingponginnovation.fitcuba.ui.detailevent.innerdetailevent

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.pingponginnovation.fitcuba.R
import kotlinx.android.synthetic.main.item_inner_detail.view.*

class InnerDetailAdapter (
    val context: Context,
    private val listener: (UiInnerDetailEvent) -> Unit,
    var aInnerPlace: List<UiInnerDetailEvent> = ArrayList()
) : RecyclerView.Adapter<InnerDetailAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_inner_detail, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(aInnerPlace[position], listener)
    }

    override fun getItemCount(): Int = aInnerPlace.size

    fun populateEvents(aInnerPlace: List<UiInnerDetailEvent>) {
        this.aInnerPlace = aInnerPlace
        notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(event: UiInnerDetailEvent, listener: (UiInnerDetailEvent) -> Unit) = with(itemView) {
            Glide.with(context).load(event.backgroundImage).into(im_icon)
            txt_title.text = event.title
            txt_description.text = event.description
            txt_complement.text = event.location
            setOnClickListener { listener(event) }
        }
    }
}