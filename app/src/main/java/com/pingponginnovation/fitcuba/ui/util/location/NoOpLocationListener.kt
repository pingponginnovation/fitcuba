package com.pingponginnovation.fitcuba.ui.util.location

import android.location.Location
import android.location.LocationListener
import android.os.Bundle

class NoOpLocationListener : LocationListener {
    override fun onLocationChanged(location: Location) {
        // no-op
    }

    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {
        // no-op
    }

    override fun onProviderEnabled(provider: String) {
        // no-op
    }

    override fun onProviderDisabled(provider: String) {
        // no-op
    }
}