package com.pingponginnovation.fitcuba.ui.core.home

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.pingponginnovation.fitcuba.R
import kotlinx.android.synthetic.main.item_home_place.view.*

class HomeAdapter (
    val context: Context,
    private val listener: (UiHome) -> Unit,
    var aPlaces: List<UiHome> = ArrayList()
) : RecyclerView.Adapter<HomeAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_home_place, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(aPlaces[position], listener)
    }

    override fun getItemCount(): Int = aPlaces.size

    fun populateEvents(aPlaces: List<UiHome>) {
        this.aPlaces = aPlaces
        notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(place: UiHome, listener: (UiHome) -> Unit) = with(itemView) {
            Glide.with(context).load(place.backgroundImage).into(img_background)
            Glide.with(context).load(place.weatherImage).into(img_weather)
            txt_weather.text = place.weather
            txt_title.text = place.title
            txt_description.text = place.description
            setOnClickListener { listener(place) }
        }
    }
}