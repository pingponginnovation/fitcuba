package com.pingponginnovation.fitcuba.ui.core.home

import android.arch.lifecycle.ViewModel
import com.pingponginnovation.fitcuba.data.ResultData
import com.pingponginnovation.fitcuba.data.core.HomeRepository
import com.pingponginnovation.fitcuba.data.login.LoginRepository
import io.reactivex.Observable
import io.reactivex.functions.Function3
import javax.inject.Inject

class HomeViewModel @Inject
internal constructor(private val homeRepository: HomeRepository, private val uiMapper: UiHomeMapper, private val loginRepository: LoginRepository) : ViewModel() {

    fun getHomeData(): Observable<ResultData<List<UiHome>>>{
        return Observable.zip(
            homeRepository.getAllData(),
            homeRepository.getWeather("Trinidad,cu"),
            homeRepository.getWeather("Viñales,cu"),
            Function3 { resultData, trinidadWeather, vinalesWeather ->
                uiMapper.toUiHome(resultData, 250.0, trinidadWeather, vinalesWeather) }
        )
    }

    fun clearSession() {
        loginRepository.clearSession()
    }
}