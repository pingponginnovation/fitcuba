package com.pingponginnovation.fitcuba.ui.detailevent

import java.io.Serializable

data class UiNotification(
    val idBeacon: Int,
    val title: String,
    val body: String,
    val idPlace: Int,
    val idCity: Int,
    val idEvent: Int
) : Serializable