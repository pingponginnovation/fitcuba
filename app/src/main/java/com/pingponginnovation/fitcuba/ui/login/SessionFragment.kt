package com.pingponginnovation.fitcuba.ui.login

import butterknife.OnClick
import com.metova.slim.annotation.Callback
import com.metova.slim.annotation.Layout
import com.pingponginnovation.fitcuba.R
import com.pingponginnovation.fitcuba.module.ApplicationComponentHolder
import com.pingponginnovation.fitcuba.ui.base.ViewModelFragment
import com.pingponginnovation.fitcuba.ui.navigation.Navigation
import com.pingponginnovation.fitcuba.ui.navigation.UiAction

@Layout(R.layout.fragment_sesion)
class SessionFragment : ViewModelFragment<LoginViewModel>() {

    @Callback
    lateinit var navigation: Navigation

    override fun viewModelClass() = LoginViewModel::class.java

    override fun inject() = ApplicationComponentHolder.INSTANCE.getComponent().inject(this)

    @OnClick(R.id.btn_signup)
    fun goToSignUp() {
        navigation.navigateToAction(UiAction.Register)
    }

    @OnClick(R.id.btn_signin)
    fun goToSignIn() {
        navigation.navigateToAction(UiAction.SignIn)
    }
}
