package com.pingponginnovation.fitcuba.ui.slider

import java.io.Serializable

data class UiSliderInformation (
    val colorBackground: Int,
    val image: Int,
    val title: Int,
    val description: Int
) : Serializable