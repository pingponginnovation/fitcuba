package com.pingponginnovation.fitcuba.ui.core

import android.arch.lifecycle.ViewModel
import com.pingponginnovation.fitcuba.data.core.HomeRepository
import com.pingponginnovation.fitcuba.ui.core.home.UiHomeMapper
import com.pingponginnovation.fitcuba.ui.detailevent.innerdetailevent.UiInnerDetailEvent
import io.reactivex.Observable
import javax.inject.Inject

class CoreViewModel @Inject
internal constructor(
    private val homeRepository: HomeRepository,
    private val uiMapper: UiHomeMapper
) : ViewModel() {

    fun getDetail(idCity: Int, idPlace: Int, idEvent: Int): Observable<UiInnerDetailEvent> {
        return homeRepository.getCity(idCity)
            .map { uiMapper.toUiInnerDetailEventSingle(it, idPlace, idEvent) }
            .toObservable()
    }

}