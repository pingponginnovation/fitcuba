package com.pingponginnovation.fitcuba.ui.detailevent

import com.pingponginnovation.fitcuba.ui.detailevent.innerdetailevent.UiSurvey

class SurveyItem
internal constructor(val surveys: List<UiSurvey>) : UiDetailEventItem {

    override fun getType(): UiDetailEventItem.Type {
        return UiDetailEventItem.Type.Survey
    }
}