package com.pingponginnovation.fitcuba.ui.detailevent

interface UiDetailEventItem {

    enum class Type {
        Description,
        Survey,
        Location
    }

    fun getType(): Type
}