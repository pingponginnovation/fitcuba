package com.pingponginnovation.fitcuba.ui.core.home

import java.io.Serializable

data class UiHome (
    val idEvent: Int,
    val backgroundImage: Int,
    val weather: String,
    val weatherImage: Int,
    val title: String,
    val description: String
) : Serializable