package com.pingponginnovation.fitcuba.ui.util.forms

enum class FormField {
    Birthday,
    Country,
    Email,
    Password,
    PasswordConfirm,
    User
}