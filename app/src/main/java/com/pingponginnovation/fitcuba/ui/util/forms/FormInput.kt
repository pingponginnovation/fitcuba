package com.pingponginnovation.fitcuba.ui.util.forms

data class FormInput(val field: FormField, val value: String, val compareValue: String = "")