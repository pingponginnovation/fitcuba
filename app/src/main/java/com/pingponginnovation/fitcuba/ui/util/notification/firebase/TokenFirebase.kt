package com.pingponginnovation.fitcuba.ui.util.notification.firebase

import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService

class TokenFirebase : FirebaseInstanceIdService() {

    override fun onTokenRefresh() {
        super.onTokenRefresh()
        //Se actualiza el token
        val refreshToken = FirebaseInstanceId.getInstance().token
    }
}
