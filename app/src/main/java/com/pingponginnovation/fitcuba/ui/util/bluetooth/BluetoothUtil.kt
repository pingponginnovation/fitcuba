package com.pingponginnovation.fitcuba.ui.util.bluetooth

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Handler
import android.os.Parcelable
import com.pingponginnovation.fitcuba.R
import com.pingponginnovation.fitcuba.ui.base.BaseActivity
import com.pingponginnovation.fitcuba.ui.base.BaseFragment
import com.pingponginnovation.fitcuba.ui.util.ToastUtil
import com.pingponginnovation.fitcuba.ui.util.notification.NotificationUtil
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BluetoothUtil @Inject
internal constructor(private val context: Context, private val toastUtil: ToastUtil) {

    @Inject
    lateinit var notificationUtil: NotificationUtil

    private lateinit var bluetoothAdapter: BluetoothAdapter
    private var nameDeviceBluethoot = ""
    private var distance: Double = 0.0
    private var messageBeacon = ""
    private val RequestEnabledBluetooth = 100
    private lateinit var  baseActivity: BaseActivity

    fun configurateBluetooth(baseActivity: BaseActivity) {
        this.baseActivity = baseActivity
        if(supportBluetooth()) {
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
            requestEnableBluetooth()
        } else {
            toastUtil.showToast(baseActivity, R.string.bluetooth_not_supported)
        }
    }

    fun configurateBluetooth(baseFragment: BaseFragment) {
        if(supportBluetooth()) {
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
            requestEnableBluetooth(baseFragment)
        } else {
            baseFragment.activity?.let {
                toastUtil.showToast(it, R.string.bluetooth_not_supported)
            }
        }
    }

    private fun supportBluetooth(): Boolean {
        val packageManger = context.packageManager
        return packageManger.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH)
    }

    private fun requestEnableBluetooth() {
        if (!bluetoothAdapter.isEnabled) {
            val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            baseActivity.startActivityForResult(enableIntent, RequestEnabledBluetooth)
        } else {
            startSearchDeviceBluethoot()
        }
    }

    private fun requestEnableBluetooth(baseFragment: BaseFragment) {
        if (!bluetoothAdapter.isEnabled) {
            val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            baseFragment.startActivityForResult(enableIntent, RequestEnabledBluetooth)
        } else {
            startSearchDeviceBluethoot(baseFragment)
        }
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RequestEnabledBluetooth && resultCode != 0) {
            configurateBluetooth(baseActivity)
            startSearchDeviceBluethoot()
        }
    }

    fun onActivityResult(baseFragment: BaseFragment, requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RequestEnabledBluetooth && resultCode != 0) {
            configurateBluetooth(baseFragment)
            startSearchDeviceBluethoot(baseFragment)
        }
    }

    private fun startSearchDeviceBluethoot() {
        bluetoothAdapter.startDiscovery()
    }

    private fun startSearchDeviceBluethoot(baseFragment: BaseFragment) {
        bluetoothAdapter.startDiscovery()
        registerOwnReceiver(baseFragment)
    }

    fun registerOwnReceiver(){
        //This should be implemented onResume as well
        baseActivity.registerReceiver(blReceiver, IntentFilter(BluetoothDevice.ACTION_FOUND))
        baseActivity.registerReceiver(blReceiver, IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED))
    }

    fun registerOwnReceiver(baseFragment: BaseFragment){
        //This should be implemented onResume as well
        baseFragment.activity?.let {
            it.registerReceiver(blReceiver, IntentFilter(BluetoothDevice.ACTION_FOUND))
            it.registerReceiver(blReceiver, IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED))
        }
    }

    val blReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        val SEARCH_BLUETHOOT = 30_000L
        var handler = Handler()
        var runnable = Runnable {
            bluetoothAdapter.startDiscovery()
        }
        override fun onReceive(context: Context, intent: Intent) {
            //This could be applicate like this fitcuba

            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                setDistanceAndNameBluethoot(intent.getParcelableExtra<Parcelable>(BluetoothDevice.EXTRA_DEVICE) as BluetoothDevice, intent)
                if (nameDeviceBluethoot != ""){
                    notificationUtil.shouldCreateNotification(true, nameDeviceBluethoot, baseActivity)
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED == action) {
                handler.postDelayed(runnable, SEARCH_BLUETHOOT)
            }
        }

        private fun setDistanceAndNameBluethoot(device: BluetoothDevice, intent: Intent) {
            try {
                distance = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, java.lang.Short.MIN_VALUE).toDouble()
                if(device.name != null ){
                    nameDeviceBluethoot = device.name.substring(device.name.length - 6)
                }
                Timber.d("El nombre es $nameDeviceBluethoot")
            } catch (exception: NullPointerException) {
            } catch (exception: StringIndexOutOfBoundsException) {
            }

        }
    }
}