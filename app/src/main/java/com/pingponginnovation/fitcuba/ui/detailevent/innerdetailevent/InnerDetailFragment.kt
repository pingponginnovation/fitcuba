package com.pingponginnovation.fitcuba.ui.detailevent.innerdetailevent

import android.location.Location
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import butterknife.BindView
import butterknife.OnClick
import com.metova.slim.annotation.Callback
import com.metova.slim.annotation.Layout
import com.pingponginnovation.fitcuba.R
import com.pingponginnovation.fitcuba.module.ApplicationComponentHolder
import com.pingponginnovation.fitcuba.ui.base.ViewModelFragment
import com.pingponginnovation.fitcuba.ui.detailevent.DetailEventViewModel
import com.pingponginnovation.fitcuba.ui.navigation.Navigation
import com.pingponginnovation.fitcuba.ui.navigation.UiAction
import com.pingponginnovation.fitcuba.ui.util.Keyboard
import com.pingponginnovation.fitcuba.ui.util.SnackbarUtil
import com.pingponginnovation.fitcuba.ui.util.location.LocationUtil
import com.pingponginnovation.fitcuba.ui.util.location.NoOpLocationListener
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_inner_detail.*
import javax.inject.Inject

@Layout(R.layout.fragment_inner_detail)
class InnerDetailFragment  : ViewModelFragment<DetailEventViewModel>() {

    @BindView(R.id.rv_event) lateinit var rvEvent: RecyclerView

    @Callback
    lateinit var navigation: Navigation

    @Inject
    lateinit var snackbarUtil: SnackbarUtil

    @Inject
    lateinit var locationUtil: LocationUtil

    private var adapter: InnerDetailAdapter? = null
    private val locationListener = NoOpLocationListener()
    private var location: Location? = null

    private val TotalColumns = 1
    private var currentFilter = 1

    override fun viewModelClass() = DetailEventViewModel::class.java

    override fun inject() = ApplicationComponentHolder.INSTANCE.getComponent().inject(this)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        handleColorsOfImages(currentFilter)
    }

    override fun subscribeOnStart() {
        super.subscribeOnStart()

        if(locationUtil.hasLocationPermission()) {
            locationUtil.startLocationUpdates(locationListener)
            location = locationUtil.lastLocation
        }
        addSubscription(viewModel()!!.getDetailInnerData()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::setAdapter))
        viewModel()!!.getInnerData(currentFilter, location)
    }

    private fun setAdapter(uiEvent: List<UiInnerDetailEvent>) {
        if(uiEvent.isEmpty()) {
            snackbarUtil.showLongSnackbar(view!!, "Sin resultado")
        }
        rvEvent.setHasFixedSize(true)
        rvEvent.layoutManager = GridLayoutManager(context, TotalColumns)
        adapter = InnerDetailAdapter(context!!, {
            navigateToInnerDetailEvent(it)
        })
        rvEvent.adapter = adapter
        adapter!!.populateEvents(uiEvent)
    }

    fun navigateToInnerDetailEvent(argument: UiInnerDetailEvent) {
        navigation.navigateToAction(UiAction.DetailEvent, argument)
    }

    @OnClick(R.id.im_back)
    fun navigationBack() {
        navigation.navigateBack()
    }

    @OnClick(R.id.im_places)
    fun filterByPlaces() {
        currentFilter = 1
        handleColorsOfImages(currentFilter)
        viewModel()!!.getInnerData(currentFilter, location)
    }

    @OnClick(R.id.im_services)
    fun filterByServices() {
        currentFilter = 2
        handleColorsOfImages(currentFilter)
        viewModel()!!.getInnerData(currentFilter, location)
    }

    @OnClick(R.id.im_events)
    fun filterByEvents() {
        currentFilter = 3
        handleColorsOfImages(currentFilter)
        viewModel()!!.getInnerData(currentFilter, location)
    }

    @OnClick(R.id.img_search)
    fun filterBySearch() {
        Keyboard.close(requireActivity(), view!!)
        viewModel()!!.makeFilter(edt_search.text.toString().toLowerCase())
    }

    private fun handleColorsOfImages(typeSearch: Int) {
        when (typeSearch) {
            1 -> {
                im_places.setColorFilter(ContextCompat.getColor(context!!, R.color.blue_50))
                im_services.setColorFilter(ContextCompat.getColor(context!!, R.color.gray_70))
                im_events.setColorFilter(ContextCompat.getColor(context!!, R.color.gray_70))
            }
            2 -> {
                im_places.setColorFilter(ContextCompat.getColor(context!!, R.color.gray_70))
                im_services.setColorFilter(ContextCompat.getColor(context!!, R.color.blue_50))
                im_events.setColorFilter(ContextCompat.getColor(context!!, R.color.gray_70))
            }
            else -> {
                im_places.setColorFilter(ContextCompat.getColor(context!!, R.color.gray_70))
                im_services.setColorFilter(ContextCompat.getColor(context!!, R.color.gray_70))
                im_events.setColorFilter(ContextCompat.getColor(context!!, R.color.blue_50))
            }
        }
    }

    override fun onPause() {
        super.onPause()

        locationUtil.removeUpdates(locationListener);
    }
}