package com.pingponginnovation.fitcuba.ui.util.location

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LocationUtil @Inject
constructor(private val context: Context) {

    val locationService: LocationManager?
        get() = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager

    val lastLocation: Location?
        @SuppressLint("MissingPermission")
        get() {
            val locationManager = locationService
            return if (locationManager != null && hasLocationPermission()) {
                locationManager.getLastKnownLocation(LOCATION_PROVIDER)
            } else null
        }

    val isLocationActivate: Boolean
        get() {
            val locationManager = locationService
            return locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)
        }

    fun hasLocationPermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            context,
            LOCATION_PERMISSION
        ) == PackageManager.PERMISSION_GRANTED
    }

    @SuppressLint("MissingPermission")
    fun startLocationUpdates(locationListener: LocationListener): LocationManager? {
        val locationManager = locationService
        if (locationManager != null && hasLocationPermission()) {
            locationManager.requestLocationUpdates(LOCATION_PROVIDER, 0, 0f, locationListener)
        }
        return locationManager
    }

    fun removeUpdates(locationListener: LocationListener) {
        val locationManager = locationService
        locationManager?.removeUpdates(locationListener)
    }

    fun requestPermission(activity: Activity) {
        ActivityCompat.requestPermissions(activity, arrayOf(LOCATION_PERMISSION), CODE_LOCATION)
    }

    fun isPermissionAccepted(grantResults: IntArray): Boolean {
        return grantResults[0] != PackageManager.PERMISSION_DENIED
    }

    companion object {

        internal val LOCATION_PERMISSION: String
        internal val LOCATION_PROVIDER: String
        internal val CODE_LOCATION = 100

        init {
            LOCATION_PERMISSION = Manifest.permission.ACCESS_FINE_LOCATION
            LOCATION_PROVIDER = LocationManager.GPS_PROVIDER
        }
    }
}
