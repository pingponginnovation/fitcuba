package com.pingponginnovation.fitcuba.ui.login.register

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import butterknife.OnClick
import com.pingponginnovation.fitcuba.ui.navigation.Navigation
import com.metova.slim.annotation.Callback
import com.metova.slim.annotation.Layout
import com.pingponginnovation.fitcuba.R
import com.pingponginnovation.fitcuba.module.ApplicationComponentHolder
import com.pingponginnovation.fitcuba.ui.base.ViewModelFragment
import com.pingponginnovation.fitcuba.ui.login.UiLoginResult
import com.pingponginnovation.fitcuba.ui.navigation.UiAction
import com.pingponginnovation.fitcuba.ui.util.DialogDateUtil
import com.pingponginnovation.fitcuba.ui.util.LoadingDialogFragment
import com.pingponginnovation.fitcuba.ui.util.SnackbarUtil
import com.pingponginnovation.fitcuba.ui.util.forms.FormError
import com.pingponginnovation.fitcuba.ui.util.forms.FormField
import com.pingponginnovation.fitcuba.ui.util.forms.FormInput
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_register.*
import javax.inject.Inject

@Layout(R.layout.fragment_register)
class RegisterFragment : ViewModelFragment<RegisterViewModel>(), AdapterView.OnItemSelectedListener {

    @Callback
    lateinit var navigation: Navigation

    @Inject
    lateinit var dialogDateUtil: DialogDateUtil

    @Inject
    lateinit var snackbarUtil: SnackbarUtil

    @Inject
    lateinit var loadingDialog: LoadingDialogFragment

    lateinit var listenerDate: DatePickerDialog.OnDateSetListener

    override fun viewModelClass() = RegisterViewModel::class.java

    override fun inject() = ApplicationComponentHolder.INSTANCE.getComponent().inject(this)

    override fun subscribeOnStart() {
        super.subscribeOnStart()

        loadingDialog.show(requireFragmentManager(), null)
        addSubscription(viewModel()!!.errorUpdates()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::handleFieldErrors))
        addSubscription(viewModel()!!.getSignUpResult()
            .observeOn(AndroidSchedulers.mainThread())
            .flatMap { result -> loadingDialog.loadFinishedObservable(result) }
            .subscribe(this::handleRegisterResult))
        addSubscription(viewModel()!!.getCountries()
            .observeOn(AndroidSchedulers.mainThread())
            .flatMap { result -> loadingDialog.loadFinishedSingle(result) }
            .subscribe(this::setAdapterCountries))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listenerDate = DatePickerDialog.OnDateSetListener { _, year, month, day -> setDate(year, month, day) }

        setUpMaps()
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        //no-op
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        //no-op
    }

    private fun setUpMaps() {
        edt_user.setTag(R.id.field_type_key, FormField.Email)
        edt_password.setTag(R.id.field_type_key, FormField.Password)
        edt_birth_date.setTag(R.id.field_type_key, FormField.Birthday)
        sp_country.setTag(R.id.field_type_key, FormField.Country)
    }

    private fun handleFieldErrors(fieldError: FormError) {
        snackbarUtil.showLongSnackbar(view!!, getString(fieldError.error, fieldError.fieldText))
    }

    private fun handleRegisterResult(uiLoginResult: UiLoginResult){
        when (uiLoginResult) {
            UiLoginResult.Success -> navigation.navigateToAction(UiAction.Core)
            UiLoginResult.InvalidCredentials -> snackbarUtil.showSnackbar(view!!, R.string.user_invalid)
            UiLoginResult.ForbiddenError -> snackbarUtil.showSnackbar(view!!, R.string.forbidden)
            UiLoginResult.ConnectionError -> snackbarUtil.showSnackbar(view!!, R.string.connection_error)
            UiLoginResult.GenericError -> snackbarUtil.showSnackbar(view!!, R.string.generic_request_error)
        }
    }

    private fun setAdapterCountries(countries: List<String>) {
        context?.let {
            val adapter = ArrayAdapter(it, R.layout.item_sp_country_country, countries)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            sp_country.adapter = adapter
        }
        sp_country.onItemSelectedListener = this
    }

    private fun setDate( year: Int,month: Int, day: Int) {
        edt_birth_date.setText(dialogDateUtil.formatDate(year, month, day))
        handleSubmitButtonClick()
    }

    @OnClick(R.id.edt_birth_date)
    fun setBirthday() {
        context?.let {
            dialogDateUtil.createDialogDate(it, listenerDate)
        }
    }

    @OnClick(R.id.btn_signup)
    fun handleSubmitButtonClick() {
        val country = sp_country.selectedItemId + 1
        if (viewModel()!!.validateField(FormInput(FormField.Email, edt_user.text.toString())) &&
            viewModel()!!.validateField(FormInput(FormField.Birthday, edt_birth_date.text.toString())) &&
            viewModel()!!.validateField(FormInput(FormField.Country, country.toString())))
        {
            loadingDialog.show(requireFragmentManager(), null)
            viewModel()!!.submitNewUser(
                edt_user.text.toString(),
                edt_password.text.toString(),
                edt_birth_date.text.toString(),
                country.toInt()
            )
        }
    }

    @OnClick(R.id.im_back)
    fun navigationBack() {
        navigation.navigateBack()
    }
}