package com.pingponginnovation.fitcuba.ui.core

import android.content.Intent
import android.os.Bundle
import com.metova.slim.annotation.Layout
import com.pingponginnovation.fitcuba.R
import com.pingponginnovation.fitcuba.module.ApplicationComponentHolder
import com.pingponginnovation.fitcuba.ui.base.ViewModelActivity
import com.pingponginnovation.fitcuba.ui.detailevent.UiNotification
import com.pingponginnovation.fitcuba.ui.detailevent.innerdetailevent.UiInnerDetailEvent
import com.pingponginnovation.fitcuba.ui.navigation.UiAction
import com.pingponginnovation.fitcuba.ui.util.bluetooth.BluetoothUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import timber.log.Timber
import java.lang.IllegalArgumentException
import javax.inject.Inject

@Layout(R.layout.activity_core)
class CoreActivity: ViewModelActivity<CoreViewModel>() {

    private val Beacon = "BEACON"

    @Inject
    lateinit var bluetoothUtil: BluetoothUtil

    override fun viewModelClass() = CoreViewModel::class.java

    override fun inject() = ApplicationComponentHolder.INSTANCE.getComponent().inject(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        bluetoothUtil.configurateBluetooth(this)
        bluetoothUtil.registerOwnReceiver()
        navigateToAction(UiAction.Home)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        bluetoothUtil.onActivityResult(requestCode, resultCode, data)
    }

    override fun onResume() {
        super.onResume()

        handleViewToShow()
    }

    override fun onDestroy() {
        unregister()
        super.onDestroy()
    }

    private fun unregister() {
        try {
            unregisterReceiver(bluetoothUtil.blReceiver)
        }catch (e : IllegalArgumentException){
            Timber.e("Error when unregister receiver ${e.message}")
        }
    }

    private fun handleViewToShow() {
        val serializable = intent.getSerializableExtra(Beacon)
        if(serializable != null) {
            val notification = serializable as UiNotification
            addSubscription(viewModel()!!.getDetail(notification.idCity, notification.idPlace, notification.idEvent)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::navigateToInnerDetailEvent))
        }
    }

    private fun navigateToInnerDetailEvent(argument: UiInnerDetailEvent) {
        navigateToAction(UiAction.DetailEvent, argument)
    }
}
