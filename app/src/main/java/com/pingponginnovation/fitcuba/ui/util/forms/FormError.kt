package com.pingponginnovation.fitcuba.ui.util.forms

class FormError(val field: FormField, val error: Int, var fieldText: String = "") {

    val noError = -1
}