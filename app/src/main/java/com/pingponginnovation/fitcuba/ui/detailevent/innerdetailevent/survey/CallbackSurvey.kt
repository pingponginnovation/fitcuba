package com.pingponginnovation.fitcuba.ui.detailevent.innerdetailevent.survey

interface CallbackSurvey {

    fun answer(response: Int, idQuestion: Int)

}