package com.pingponginnovation.fitcuba.ui.util

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.ButterKnife
import com.pingponginnovation.fitcuba.R
import com.pingponginnovation.fitcuba.module.ApplicationComponentHolder
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LoadingDialogFragment @Inject
internal constructor(): DialogFragment() {

    private val MinimumDisplayTime: Long = 700 // in milliseconds

    private var startTime: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        ApplicationComponentHolder.getInstance().getComponent().inject(this)
        super.onCreate(savedInstanceState)

        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.AppTheme)
        isCancelable = false
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_loading_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ButterKnife.bind(this, view)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)

        initTimer()
    }

    private fun initTimer() {
        startTime = System.currentTimeMillis()
    }

    fun dismissInAnyCycle() {
        super.dismiss()
    }

    override fun dismiss() {
        if(isResumed) {
            super.dismiss()
        }
    }

    override fun dismissAllowingStateLoss() {
        if(isResumed) {
            super.dismissAllowingStateLoss()
        }
    }

    fun <E> loadFinishedObservable(value: E): Observable<E> {
        val timeRemaining = getTimeRemaining()

        return Observable.just(value)
            .delay(timeRemaining, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .doAfterNext { dismiss() }
    }

    fun <E> loadFinishedSingle(value: E): Single<E> {
        return loadFinishedObservable(value).firstOrError()
    }

    private fun getTimeRemaining(): Long {
        val timeRemaining = MinimumDisplayTime - (System.currentTimeMillis() - startTime)
        return Math.max(timeRemaining, 0)
    }
}
