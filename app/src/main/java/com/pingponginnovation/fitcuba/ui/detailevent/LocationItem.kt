package com.pingponginnovation.fitcuba.ui.detailevent

class LocationItem internal constructor(
    val latitude: Double,
    val longitude: Double,
    val name: String,
    val shouldHideButton: Boolean
) : UiDetailEventItem {

    override fun getType(): UiDetailEventItem.Type {
        return UiDetailEventItem.Type.Location
    }
}