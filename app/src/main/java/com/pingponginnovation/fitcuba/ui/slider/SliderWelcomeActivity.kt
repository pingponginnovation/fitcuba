package com.pingponginnovation.fitcuba.ui.slider

import android.os.Bundle
import android.view.View
import butterknife.OnClick
import com.metova.slim.annotation.Layout
import com.pingponginnovation.fitcuba.R
import com.pingponginnovation.fitcuba.module.ApplicationComponentHolder
import com.pingponginnovation.fitcuba.ui.base.ViewModelActivity
import com.pingponginnovation.fitcuba.ui.navigation.UiAction
import com.pingponginnovation.fitcuba.ui.splash.SplashScreenViewModel
import kotlinx.android.synthetic.main.activity_slide_welcome.*

@Layout(R.layout.activity_slide_welcome)
class SliderWelcomeActivity: ViewModelActivity<SplashScreenViewModel>(), CallbackSlider {

    override fun viewModelClass() = SplashScreenViewModel::class.java

    override fun inject() = ApplicationComponentHolder.getInstance().getComponent().inject(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setAdapter()
    }

    override fun handleViewButton(title: Int) {
        if(title == R.string.slider_title_tourism) {
            btn_start.visibility =  View.VISIBLE
        } else {
            btn_start.visibility =  View.GONE
        }
    }

    private fun setAdapter() {
        val adapter = SliderAdapter(supportFragmentManager, this)
        pager.adapter = adapter
        tabDots.setupWithViewPager(pager, true)
    }

    @OnClick(R.id.btn_start)
    fun goToSession() {
        viewModel().saveStatusSlider()
        navigateToAction(UiAction.Login)
    }
}