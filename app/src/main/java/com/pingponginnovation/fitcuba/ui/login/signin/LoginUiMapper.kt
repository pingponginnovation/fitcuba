package com.pingponginnovation.fitcuba.ui.login.signin

import com.pingponginnovation.fitcuba.data.login.LoginData
import com.pingponginnovation.fitcuba.data.login.LoginType
import com.pingponginnovation.fitcuba.ui.login.UiLoginResult
import javax.inject.Inject

class LoginUiMapper @Inject
internal constructor() {

    fun toUiSignInResult(loginData: LoginData): UiLoginResult {
        return when (loginData.type) {
            LoginType.Success -> UiLoginResult.Success
            LoginType.InvalidCredentials -> UiLoginResult.InvalidCredentials
            LoginType.ForbiddenError -> UiLoginResult.ForbiddenError
            LoginType.ConnectionError -> UiLoginResult.ConnectionError
            LoginType.GenericError -> UiLoginResult.GenericError
        }
    }
}