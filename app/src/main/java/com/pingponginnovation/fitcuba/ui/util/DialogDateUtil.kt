package com.pingponginnovation.fitcuba.ui.util

import android.R
import android.app.DatePickerDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DialogDateUtil @Inject
internal constructor() {

    fun createDialogDate(context: Context, listenerDate: DatePickerDialog.OnDateSetListener) {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val dialogDate = DatePickerDialog(context, R.style.Theme_Holo_Light_Dialog_MinWidth,
            listenerDate, year, month, day)
        calendar.add(Calendar.YEAR, -12)
        dialogDate.datePicker.maxDate = calendar.timeInMillis
        dialogDate.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialogDate.show()
    }

    fun formatDate(year: Int,month: Int, day: Int): String {
        var month = month
        val monthString:String
        month += 1
        monthString = if (month < 10)
            "0$month"
        else
            month.toString()
        return if(day < 10){
            "$year/$monthString/0$day"
        } else {
            "$year/$monthString/$day"
        }
    }
}