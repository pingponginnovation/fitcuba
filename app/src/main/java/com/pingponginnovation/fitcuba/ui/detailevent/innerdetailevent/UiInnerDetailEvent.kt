package com.pingponginnovation.fitcuba.ui.detailevent.innerdetailevent

import java.io.Serializable

data class UiInnerDetailEvent (
    val idEvent: Int,
    val idPlace: Int,
    val backgroundImage: Int,
    val descriptionImage: Int,
    val weather: String,
    val location: String,
    val title: String,
    val description: String,
    val complement: String,
    val latitude: String,
    val longitude: String,
    val surveys: List<UiSurvey>,
    val tags: List<String>
): Serializable