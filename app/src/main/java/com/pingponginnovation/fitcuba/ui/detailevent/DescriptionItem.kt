package com.pingponginnovation.fitcuba.ui.detailevent

class DescriptionItem : UiDetailEventItem {

    val description: String
    val image: Int
    val subDescription: String

    internal constructor(description: String) {
        this.description = description
        this.image = 0
        this.subDescription = ""
    }

    internal constructor(description: String, image: Int, subDescription: String) {
        this.description = description
        this.image = image
        this.subDescription = subDescription
    }

    override fun getType(): UiDetailEventItem.Type {
        return UiDetailEventItem.Type.Description
    }
}