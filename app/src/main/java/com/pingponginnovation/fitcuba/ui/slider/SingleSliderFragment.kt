package com.pingponginnovation.fitcuba.ui.slider

import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.metova.slim.annotation.Callback
import com.metova.slim.annotation.Layout
import com.pingponginnovation.fitcuba.R
import com.pingponginnovation.fitcuba.module.ApplicationComponentHolder
import com.pingponginnovation.fitcuba.ui.base.ViewModelFragment
import com.pingponginnovation.fitcuba.ui.navigation.Navigation
import com.pingponginnovation.fitcuba.ui.splash.SplashScreenViewModel
import kotlinx.android.synthetic.main.fragment_single_slide.*

@Layout(R.layout.fragment_single_slide)
class SingleSliderFragment : ViewModelFragment<SplashScreenViewModel>() {

    companion object {

        private const val Information = "INFORMATION"
        private const val Callback = "CALLBACK"

        fun newInstance(uiSliderInformation: UiSliderInformation, callbackSlider: CallbackSlider): SingleSliderFragment{
            val fragment = SingleSliderFragment()
            val args = Bundle()
            args.putSerializable(Information, uiSliderInformation)
            args.putSerializable(Callback, callbackSlider)
            fragment.arguments = args
            return fragment
        }
    }

    @Callback
    lateinit var navigation: Navigation

    override fun viewModelClass() = SplashScreenViewModel::class.java

    override fun inject() = ApplicationComponentHolder.INSTANCE.getComponent().inject(this)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setInformation()
    }

    private fun setInformation() {
        val uiSliderInformation = arguments?.getSerializable(Information) as UiSliderInformation
        val callback = arguments?.getSerializable(Callback) as CallbackSlider
        uiSliderInformation.let {
            base_bakground.setBackgroundColor(resources.getColor(it.colorBackground))
            Glide.with(context!!).load(it.image).into(im_type_slider)
            txt_title.setText(it.title)
            txt_description.setText(it.description)
            callback.handleViewButton(it.title)
        }
    }
}