package com.pingponginnovation.fitcuba.ui.slider

import java.io.Serializable

interface CallbackSlider: Serializable {

    fun handleViewButton(title: Int)
}