package com.pingponginnovation.fitcuba.ui.detailevent

import android.content.Context
import android.support.v7.widget.RecyclerView

import android.support.annotation.NonNull
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.pingponginnovation.fitcuba.R
import com.pingponginnovation.fitcuba.ui.detailevent.innerdetailevent.survey.CallbackSurvey
import com.pingponginnovation.fitcuba.ui.detailevent.innerdetailevent.survey.SurveyAdapter
import kotlinx.android.synthetic.main.item_event_description.view.*
import kotlinx.android.synthetic.main.item_event_location.view.*
import kotlinx.android.synthetic.main.item_event_opinion.view.*
import com.google.android.gms.maps.GoogleMap

class DetailEventAdapter(
    val context: Context,
    private var aDetailEvent: List<UiDetailEventItem> = ArrayList(),
    val callback: DetailEventCallback,
    private val surveyCallback: CallbackSurvey,
    private val supportFragmentManager: FragmentManager
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var locationHolder: LocationHolder? = null

    @NonNull
    override
    fun onCreateViewHolder(@NonNull parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (UiDetailEventItem.Type.values()[viewType]) {
            UiDetailEventItem.Type.Description -> {
                val descriptionView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_event_description, parent, false)
                return DescriptionHolder(descriptionView)
            }
            UiDetailEventItem.Type.Survey -> {
                val surveyView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_event_opinion, parent, false)
                return SurveyHolder(surveyView)
            }
            UiDetailEventItem.Type.Location -> {
                val locationView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_event_location, parent, false)
                this.locationHolder = LocationHolder(locationView)
                return this.locationHolder!!
            }
            else -> throw UnsupportedOperationException()
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (UiDetailEventItem.Type.values()[holder.itemViewType]) {
            UiDetailEventItem.Type.Description -> {
                (holder as DescriptionHolder).bind(aDetailEvent[position] as DescriptionItem)
            }
            UiDetailEventItem.Type.Survey -> {
                (holder as SurveyHolder).bind(supportFragmentManager, aDetailEvent[position] as SurveyItem, surveyCallback)
            }
            UiDetailEventItem.Type.Location -> {
                (holder as LocationHolder).bind(aDetailEvent[position] as LocationItem, callback)
            }
            else -> throw UnsupportedOperationException()
        }
    }

    override fun getItemCount(): Int {
        return aDetailEvent.size
    }

    override fun getItemViewType(position: Int): Int {
        return when (aDetailEvent[position].getType()) {
            UiDetailEventItem.Type.Description -> UiDetailEventItem.Type.Description.ordinal
            UiDetailEventItem.Type.Survey -> UiDetailEventItem.Type.Survey.ordinal
            UiDetailEventItem.Type.Location -> UiDetailEventItem.Type.Location.ordinal
            else -> throw UnsupportedOperationException()
        }
    }

    class DescriptionHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(event: DescriptionItem) = with(itemView) {
            txt_description_body.text = event.description
            if(event.image == 0) {
                img_description.visibility = View.GONE
            } else {
                Glide.with(context).load(event.image).into(img_description)
            }
            if(event.subDescription.isEmpty()) {
                txt_sub_description.visibility = View.GONE
            } else {
                txt_sub_description.text = event.subDescription
            }
        }
    }

    class SurveyHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(supportFragmentManager: FragmentManager, surveys: SurveyItem, surveyCallback: CallbackSurvey) = with(itemView) {
            val adapter = SurveyAdapter(supportFragmentManager, surveys.surveys,  surveyCallback)
            pager.adapter = adapter
            tabDots.setupWithViewPager(pager, true)
        }
    }

    class LocationHolder(view: View) : RecyclerView.ViewHolder(view), OnMapReadyCallback {

        lateinit var mapViewInner: MapView
        var map: GoogleMap? =null
        private lateinit var locationInner: LocationItem
        private var onMapReadyCallback = this
        private lateinit var contextInner: Context


        fun bind(location: LocationItem, callback: DetailEventCallback) = with(itemView) {
            contextInner = context
            mapViewInner = map
            mapViewInner.onCreate(null)
            mapViewInner.getMapAsync(onMapReadyCallback)
            if(location.shouldHideButton) {
                btn_explore.visibility = View.GONE
            } else {
                btn_explore.setOnClickListener { callback.goToInnerDetail() }
            }
            locationInner = location
        }

        override fun onMapReady(googleMap: GoogleMap?) {
            MapsInitializer.initialize(contextInner)
            map = googleMap
            map?.uiSettings?.isMyLocationButtonEnabled = false

            setLocation(locationInner.latitude, locationInner.longitude, locationInner.name)
        }

        private fun setLocation(latitude: Double, longitude: Double, name : String) {
            if(map == null) {
                return
            }
            val locationPlace = LatLng(latitude, longitude)
            map!!.addMarker(MarkerOptions().position(locationPlace).title(name))
            map!!.animateCamera(CameraUpdateFactory.newLatLngZoom(locationPlace, 15.0f))
        }

        fun onResume() {
            mapViewInner.onResume()
        }

        fun onPause() {
            mapViewInner.onPause()
        }

        fun onDestroy() {
            mapViewInner.onDestroy()
        }
    }
}