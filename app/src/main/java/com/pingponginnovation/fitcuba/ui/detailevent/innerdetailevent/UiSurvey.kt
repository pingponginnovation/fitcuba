package com.pingponginnovation.fitcuba.ui.detailevent.innerdetailevent

import java.io.Serializable

data class UiSurvey(val idSurvey: Int,
                    val placeId: Int,
                    val question: String,
                    val colorBackground: Int) : Serializable