package com.pingponginnovation.fitcuba.ui.detailevent.innerdetailevent.survey

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.pingponginnovation.fitcuba.R
import com.pingponginnovation.fitcuba.ui.detailevent.innerdetailevent.UiSurvey

class SurveyAdapter
internal constructor(fragmentManager: FragmentManager,
                     val surveys: List<UiSurvey>,
                     private val callbackSurvey: CallbackSurvey) : FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(currentFragment: Int): Fragment {
        var uiSurvey: UiSurvey? = null
        when(currentFragment){
            0 -> uiSurvey = UiSurvey(surveys[currentFragment].idSurvey, surveys[currentFragment].placeId, surveys[currentFragment].question, R.color.blue_40)
            1 -> uiSurvey = UiSurvey(surveys[currentFragment].idSurvey, surveys[currentFragment].placeId, surveys[currentFragment].question, R.color.pink_40)
            2 -> uiSurvey = UiSurvey(surveys[currentFragment].idSurvey, surveys[currentFragment].placeId, surveys[currentFragment].question, R.color.green_40)
            3 -> uiSurvey = UiSurvey(surveys[currentFragment].idSurvey, surveys[currentFragment].placeId, surveys[currentFragment].question, R.color.blue_40)
            4 -> uiSurvey = UiSurvey(surveys[currentFragment].idSurvey, surveys[currentFragment].placeId, surveys[currentFragment].question, R.color.pink_40)
            5 -> uiSurvey = UiSurvey(surveys[currentFragment].idSurvey, surveys[currentFragment].placeId, surveys[currentFragment].question, R.color.green_40)
            6 -> uiSurvey = UiSurvey(surveys[currentFragment].idSurvey, surveys[currentFragment].placeId, surveys[currentFragment].question, R.color.blue_40)
            7 -> uiSurvey = UiSurvey(surveys[currentFragment].idSurvey, surveys[currentFragment].placeId, surveys[currentFragment].question, R.color.pink_40)
            8 -> uiSurvey = UiSurvey(surveys[currentFragment].idSurvey, surveys[currentFragment].placeId, surveys[currentFragment].question, R.color.green_40)
        }
        return SingleSurveyFragment.newInstance(uiSurvey!!, callbackSurvey)
    }

    override fun getCount() = surveys.size
}