package com.pingponginnovation.fitcuba.ui.core.home

import android.location.Location
import com.pingponginnovation.fitcuba.R
import com.pingponginnovation.fitcuba.data.ResultData
import com.pingponginnovation.fitcuba.data.event.CityData
import com.pingponginnovation.fitcuba.network.ResponseConversions
import com.pingponginnovation.fitcuba.ui.detailevent.DescriptionItem
import com.pingponginnovation.fitcuba.ui.detailevent.LocationItem
import com.pingponginnovation.fitcuba.ui.detailevent.SurveyItem
import com.pingponginnovation.fitcuba.ui.detailevent.UiDetailEventItem
import com.pingponginnovation.fitcuba.ui.detailevent.innerdetailevent.UiInnerDetailEvent
import com.pingponginnovation.fitcuba.ui.detailevent.innerdetailevent.UiSurvey
import java.util.*
import javax.inject.Inject
import java.text.DecimalFormat

class UiHomeMapper @Inject
internal constructor(private val responseConversions: ResponseConversions)  {

    fun toUiHome(cityData: ResultData<List<CityData>>, weatherCayoLargo: Double, weatherTrinidad: Double,
                 weatherVinales: Double): ResultData<List<UiHome>> {
        val dataList = arrayListOf<UiHome>()
        dataList.add(UiHome(cityData.data!![0].idCity, R.drawable.bk_cayolargo, formatWeather(weatherCayoLargo), getCurrentImage(weatherCayoLargo), cityData.data[0].nameEvent, cityData.data[0].slogan))
        dataList.add(UiHome(cityData.data[1].idCity, R.drawable.bk_trinidad, formatWeather(weatherTrinidad), getCurrentImage(weatherTrinidad), cityData.data[1].nameEvent, cityData.data[1].slogan))
        dataList.add(UiHome(cityData.data[2].idCity, R.drawable.bk_vinales, formatWeather(weatherVinales), getCurrentImage(weatherVinales), cityData.data[2].nameEvent, cityData.data[2].slogan))
        return ResultData(dataList)
    }

    fun toUiHome(throwable: Throwable): ResultData<List<UiHome>> {
        return ResultData(responseConversions.toNetworkResult(throwable))
    }

    private fun getCurrentImage(weather: Double): Int {
        return if(weather > 180) {
            R.drawable.ic_sun
        } else {
            R.drawable.ic_cold
        }
    }

    private fun formatWeather(weather: Double): String {
        return "${weather.toString().substring(0, 2)}°"
    }

    fun toUiDetailEventItem(cityData: CityData): List<UiDetailEventItem> {
        val dataList = arrayListOf<UiDetailEventItem>()
        dataList.add(DescriptionItem(cityData.descriptionsEvent[0].textDescription!!))
        //data.add(DescriptionItem("Este evento tiene una descripcion con imagen", R.drawable.bk_trinidad, "Esto va abajito de la imagen"))
        dataList.add(LocationItem(cityData.latitude.toDouble(), cityData.longitude.toDouble(), cityData.nameEvent,false))
        return dataList
    }
    //1 PLACE - 2 SERVICE - 3 EVENT /
    fun toUiInnerDetailEvent(cityData: CityData, typeSearch: Int, location: Location?): List<UiInnerDetailEvent> {
        val dataList = arrayListOf<UiInnerDetailEvent>()
        for(place in cityData.places) {
            if(typeSearch != 3) {
                if(place.category!!.idCategory == typeSearch) {
                    val surveys = arrayListOf<UiSurvey>()
                    val tags = arrayListOf<String>()
                    for(innerSurvey in place.surveys!!) {
                        surveys.add(UiSurvey(innerSurvey.idSurvey!!, innerSurvey.placesId!!, innerSurvey.question!!, 0))
                    }
                    for(innerTag in place.tags!!) {
                        tags.add(innerTag.tagValue!!)
                    }
                    dataList.add(UiInnerDetailEvent(cityData.idCity, place.idPlace!!, place.idImage!!, place.idSecondImage!!,"", getDistanceLocation(location, place.latitude!!.toDouble(), place.longitude!!.toDouble()), place.namePlace!!, place.descriptions?.get(0)?.textDescription!!, "", place.latitude!!, place.longitude!!, surveys, tags))
                }
            } else {
                for(event in place.eventsPlace!!) {
                    val tags = arrayListOf<String>()
                    for(innerTag in place.tags!!) {
                        tags.add(innerTag.tagValue!!)
                    }
                    dataList.add(UiInnerDetailEvent(cityData.idCity, event.idItem!!, event.idImage!!, event.idSecondImage!!,"", getDistanceLocation(location, place.latitude!!.toDouble(), place.longitude!!.toDouble()), event.nameItem!!, event.descriptions?.get(0)?.textDescription!!, "", place.latitude!!, place.longitude!!, Collections.emptyList(), tags))
                }
            }
        }
        return dataList
    }

    private fun getDistanceLocation(currentLocation: Location?, placeLatitue: Double, placeLongitude: Double ): String {
        var distance = ""
        currentLocation?.let {
            val locationPlace = Location("Place")
            val formatter = DecimalFormat("#0.00")
            locationPlace.latitude = placeLatitue
            locationPlace.longitude = placeLongitude
            val distanceResult= formatter.format((it.distanceTo(locationPlace) / 1000))
            distance = "A $distanceResult km de distancia"
        }
        return distance
    }

    fun toUiInnerDetailEventSingle(cityData: CityData, idPlace: Int, idEvent: Int): UiInnerDetailEvent {
        for(place in cityData.places) {
            if(idEvent == 0) {
                if(place.idPlace == idPlace) {
                    val surveys = arrayListOf<UiSurvey>()
                    for(innerSurvey in place.surveys!!) {
                        surveys.add(UiSurvey(innerSurvey.idSurvey!!, innerSurvey.placesId!!, innerSurvey.question!!, 0))
                    }
                    return UiInnerDetailEvent(cityData.idCity, place.idPlace, place.idImage!!, place.idSecondImage!!,"", "",place.namePlace!!, place.descriptions?.get(0)?.textDescription!!, "", place.latitude!!, place.longitude!!, surveys, Collections.emptyList())
                }
            } else {
                for(event in place.eventsPlace!!) {
                    if(event.idItem == idEvent) {
                        return UiInnerDetailEvent(cityData.idCity, event.idItem, event.idImage!!, event.idSecondImage!!,"", "", event.nameItem!!, event.descriptions?.get(0)?.textDescription!!, "", place.latitude!!, place.longitude!!, Collections.emptyList(), Collections.emptyList())
                    }
                }
            }
        }
        return UiInnerDetailEvent(0, 0, 0, 0,"", "", "", "", "", "", "", Collections.emptyList(), Collections.emptyList())
    }

    fun toUiDetailEventItem(uiInnerDetailEvent: UiInnerDetailEvent): List<UiDetailEventItem> {
        val dataList = arrayListOf<UiDetailEventItem>()
        dataList.add(DescriptionItem(uiInnerDetailEvent.description, uiInnerDetailEvent.descriptionImage, ""))
        if(uiInnerDetailEvent.surveys.isNotEmpty()) {
            dataList.add(SurveyItem(uiInnerDetailEvent.surveys))
        }
        dataList.add(LocationItem(uiInnerDetailEvent.latitude.toDouble(), uiInnerDetailEvent.longitude.toDouble(), uiInnerDetailEvent.title, true))
        return dataList
    }
}