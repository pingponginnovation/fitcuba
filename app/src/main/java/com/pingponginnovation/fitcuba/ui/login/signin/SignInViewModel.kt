package com.pingponginnovation.fitcuba.ui.login.signin

import android.arch.lifecycle.ViewModel
import com.pingponginnovation.fitcuba.data.login.LoginRepository
import com.pingponginnovation.fitcuba.ui.login.UiLoginResult
import com.pingponginnovation.fitcuba.ui.util.NoOpDisposable
import com.pingponginnovation.fitcuba.ui.util.forms.*
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class SignInViewModel @Inject
internal constructor(
    private val loginRepository: LoginRepository,
    private val uiMapper: LoginUiMapper,

    private val formHelper: FormHelper
) : ViewModel(), FormCallback {

    init {
        initFormHelper()
    }

    private val resultSubject = PublishSubject.create<UiLoginResult>()
    private var disposable: Disposable = NoOpDisposable()

    private val fieldErrorSubject = PublishSubject.create<FormError>()

    fun getLoginResult(): Observable<UiLoginResult> {
        return resultSubject
    }

    private fun initFormHelper() {
        val fieldList = Arrays.asList(
            FormField.Email,
            FormField.Password
        )
        formHelper.initialize(this, fieldList)
    }

    fun validateField(input: FormInput): Boolean {
        return formHelper.validateField(input)
    }

    fun errorUpdates(): Observable<FormError> {
        return fieldErrorSubject
    }

    fun submitLoginInformation(user: String, password: String) {
        disposable.dispose()
        disposable = loginRepository.submitLoginInformation(user, password)
            .map(uiMapper::toUiSignInResult)
            .doOnError { error ->
                Timber.w(
                    "Error submitting login information: %s",
                    error.message
                )
            }
            .subscribe(resultSubject::onNext)
    }

    override fun formError(error: FormError) {
        fieldErrorSubject.onNext(error)
    }
}