package com.pingponginnovation.fitcuba.ui.login

sealed class UiLoginResult {
    object Success : UiLoginResult()
    object InvalidCredentials : UiLoginResult()
    object ForbiddenError : UiLoginResult()
    object ConnectionError : UiLoginResult()
    object GenericError : UiLoginResult()
}