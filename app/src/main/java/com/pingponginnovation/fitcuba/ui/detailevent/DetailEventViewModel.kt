package com.pingponginnovation.fitcuba.ui.detailevent

import android.arch.lifecycle.ViewModel
import android.location.Location
import com.pingponginnovation.fitcuba.data.NetworkResult
import com.pingponginnovation.fitcuba.data.core.HomeRepository
import com.pingponginnovation.fitcuba.ui.core.home.UiHomeMapper
import com.pingponginnovation.fitcuba.ui.detailevent.innerdetailevent.UiInnerDetailEvent
import dagger.Reusable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

@Reusable
class DetailEventViewModel @Inject
internal constructor(private val homeRepository: HomeRepository, private val uiMapper: UiHomeMapper) : ViewModel() {

    private var currentId = 0

    private val uiPlaceSubject = PublishSubject.create<List<UiInnerDetailEvent>>()
    private val resultSubject = PublishSubject.create<NetworkResult>()

    fun getDetailData(idEvent: Int): Single<List<UiDetailEventItem>> {
        this.currentId = idEvent
        return homeRepository.getCity(idEvent)
            .map { uiMapper.toUiDetailEventItem(it) }
    }

    fun getDetailData(uiInnerDetailEvent: UiInnerDetailEvent): Observable<List<UiDetailEventItem>> {
        return Observable.just(uiMapper.toUiDetailEventItem(uiInnerDetailEvent))
    }

    fun getDetailInnerData(): Observable<List<UiInnerDetailEvent>> {
        return uiPlaceSubject
    }

    fun getResultSurvey(): Observable<NetworkResult> {
        return resultSubject
    }

    fun getInnerData(typeSearch: Int, location: Location?) {
        homeRepository.getCity(this.currentId)
            .map { uiMapper.toUiInnerDetailEvent(it, typeSearch, location) }
            .doOnSuccess{ result -> homeRepository.saveActualResultOfInnerPlaces(result) }
            .subscribe(uiPlaceSubject::onNext)
    }

    fun sendSurvey(answer: Int, surveysId: Int) {
        homeRepository.sendSurvey(answer, surveysId)
            .doFinally { resultSubject.onNext(NetworkResult.Success) }
            .subscribe()
    }

    fun makeFilter(search: String) {
        homeRepository.makeFilter(search)
            .subscribe(uiPlaceSubject::onNext)
    }
}