package com.pingponginnovation.fitcuba.ui.login

import android.os.Bundle
import com.pingponginnovation.fitcuba.ui.navigation.UiAction
import com.metova.slim.annotation.Layout
import com.pingponginnovation.fitcuba.R
import com.pingponginnovation.fitcuba.module.ApplicationComponentHolder
import com.pingponginnovation.fitcuba.ui.base.ViewModelActivity
import io.reactivex.android.schedulers.AndroidSchedulers

@Layout(R.layout.activity_login)
class LoginActivity: ViewModelActivity<LoginViewModel>() {

    override fun viewModelClass() = LoginViewModel::class.java

    override fun inject() = ApplicationComponentHolder.INSTANCE.getComponent().inject(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        navigateToAction(UiAction.Session)
    }

    override fun subscribeOnStart() {
        super.subscribeOnStart()

        addSubscription(viewModel().sessionActive()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{ sessionActive ->
                    if(sessionActive) {
                        navigateToAction(UiAction.Core)
                    }
                })
    }
}