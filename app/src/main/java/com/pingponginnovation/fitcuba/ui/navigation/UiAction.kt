package com.pingponginnovation.fitcuba.ui.navigation

sealed class UiAction {
    object Core : UiAction()
    object DetailEvent : UiAction()
    object InnerDetail : UiAction()
    object Home : UiAction()
    object Login : UiAction()
    object SignIn : UiAction()
    object Session : UiAction()
    object Slide : UiAction()
    object SingleSlide : UiAction()
    object Register : UiAction()
}