package com.pingponginnovation.fitcuba.ui.splash

import com.pingponginnovation.fitcuba.ui.navigation.UiAction
import com.metova.slim.annotation.Layout
import com.pingponginnovation.fitcuba.R
import com.pingponginnovation.fitcuba.module.ApplicationComponentHolder
import com.pingponginnovation.fitcuba.ui.base.ViewModelActivity
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

@Layout(R.layout.activity_splash_screen)
class SplashScreenActivity: ViewModelActivity<SplashScreenViewModel>() {

    private val DelaySplash = 1L

    override fun viewModelClass() = SplashScreenViewModel::class.java

    override fun inject() = ApplicationComponentHolder.getInstance().getComponent().inject(this)

    override fun subscribeOnStart(){
        addSubscription(Completable.timer(DelaySplash, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::defineActivityToShow))
    }

    private fun defineActivityToShow() {
        if(viewModel().getStatusSlider()) {
            navigateToAction(UiAction.Slide)
        } else {
            navigateToAction(UiAction.Login)
        }
    }
}