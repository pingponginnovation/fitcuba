package com.pingponginnovation.fitcuba.ui.core.home

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import butterknife.BindView
import butterknife.OnClick
import com.metova.slim.annotation.Callback
import com.metova.slim.annotation.Layout
import com.pingponginnovation.fitcuba.R
import com.pingponginnovation.fitcuba.data.NetworkResult
import com.pingponginnovation.fitcuba.data.ResultData
import com.pingponginnovation.fitcuba.module.ApplicationComponentHolder
import com.pingponginnovation.fitcuba.ui.base.ViewModelFragment
import com.pingponginnovation.fitcuba.ui.navigation.Navigation
import com.pingponginnovation.fitcuba.ui.navigation.UiAction
import com.pingponginnovation.fitcuba.ui.util.LoadingDialogFragment
import com.pingponginnovation.fitcuba.ui.util.SnackbarUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

@Layout(R.layout.fragment_home)
class HomeFragment : ViewModelFragment<HomeViewModel>() {

    @BindView(R.id.rv_home) lateinit var rvHome: RecyclerView

    @Callback
    lateinit var navigation: Navigation

    @Inject
    lateinit var snackbarUtil: SnackbarUtil

    @Inject
    lateinit var loadingDialog: LoadingDialogFragment

    private var adapter: HomeAdapter? = null
    private val TotalColumns = 1

    override fun viewModelClass() = HomeViewModel::class.java

    override fun inject() = ApplicationComponentHolder.INSTANCE.getComponent().inject(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        loadingDialog.show(requireFragmentManager(), null)
    }

    override fun subscribeOnStart() {
        super.subscribeOnStart()

        addSubscription(viewModel()!!.getHomeData()
            .observeOn(AndroidSchedulers.mainThread())
            .flatMap { result -> loadingDialog.loadFinishedObservable(result) }
            .subscribe(this::handleResult))
    }

    fun handleResult(uiHome: ResultData<List<UiHome>>) {
        when (uiHome.networkResult) {
            NetworkResult.Success -> setAdapter(uiHome.data!!)
            NetworkResult.ConnectionError -> snackbarUtil.showSnackbar(view!!, R.string.connection_error)
            NetworkResult.GenericError -> snackbarUtil.showSnackbar(view!!, R.string.generic_request_error)
        }
    }

    fun setAdapter(uiHome: List<UiHome>) {
        rvHome.setHasFixedSize(true)
        rvHome.layoutManager = GridLayoutManager(context, TotalColumns)
        adapter = HomeAdapter(context!!, {
            navigateToDetailEvent(it)
        })
        rvHome.adapter = adapter
        adapter!!.populateEvents(uiHome)
    }

    private fun navigateToDetailEvent(argument: UiHome) {
        navigation.navigateToAction(UiAction.DetailEvent, argument)
    }

    override fun onStop() {
        super.onStop()

        loadingDialog.dismissInAnyCycle()
    }

    @OnClick(R.id.im_close_session)
    fun closeSession() {
        viewModel()!!.clearSession()
        navigation.navigateToAction(UiAction.Login)
    }
}