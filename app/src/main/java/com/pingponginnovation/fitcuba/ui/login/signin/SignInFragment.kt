package com.pingponginnovation.fitcuba.ui.login.signin

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import butterknife.OnClick
import butterknife.OnEditorAction
import com.metova.slim.annotation.Callback
import com.metova.slim.annotation.Layout
import com.pingponginnovation.fitcuba.R
import com.pingponginnovation.fitcuba.module.ApplicationComponentHolder
import com.pingponginnovation.fitcuba.ui.base.ViewModelFragment
import com.pingponginnovation.fitcuba.ui.login.UiLoginResult
import com.pingponginnovation.fitcuba.ui.navigation.Navigation
import com.pingponginnovation.fitcuba.ui.navigation.UiAction
import com.pingponginnovation.fitcuba.ui.util.Keyboard
import com.pingponginnovation.fitcuba.ui.util.LoadingDialogFragment
import com.pingponginnovation.fitcuba.ui.util.SnackbarUtil
import com.pingponginnovation.fitcuba.ui.util.forms.FormError
import com.pingponginnovation.fitcuba.ui.util.forms.FormField
import com.pingponginnovation.fitcuba.ui.util.forms.FormInput
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_sign_in.*
import javax.inject.Inject

@Layout(R.layout.fragment_sign_in)
class SignInFragment : ViewModelFragment<SignInViewModel>() {

    @Callback
    lateinit var navigation: Navigation

    @Inject
    lateinit var snackbarUtil: SnackbarUtil

    var loadingDialog = LoadingDialogFragment()

    override fun viewModelClass() = SignInViewModel::class.java

    override fun inject() = ApplicationComponentHolder.INSTANCE.getComponent().inject(this)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpMaps()
    }

    override fun subscribeOnStart() {
        super.subscribeOnStart()

        addSubscription(viewModel()!!.errorUpdates()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::handleFieldErrors))
        addSubscription(viewModel()!!.getLoginResult()
            .observeOn(AndroidSchedulers.mainThread())
            .flatMap { result -> loadingDialog.loadFinishedObservable(result) }
            .subscribe(this::handleLoginResult))
    }

    private fun handleFieldErrors(fieldError: FormError) {
        snackbarUtil.showLongSnackbar(view!!, getString(fieldError.error, fieldError.fieldText))
    }

    private fun handleLoginResult(uiSignInResult: UiLoginResult){
        when (uiSignInResult) {
            UiLoginResult.Success -> navigation.navigateToAction(UiAction.Core)
            UiLoginResult.InvalidCredentials -> snackbarUtil.showSnackbar(view!!, R.string.user_invalid)
            UiLoginResult.ConnectionError -> snackbarUtil.showSnackbar(view!!, R.string.connection_error)
            UiLoginResult.GenericError -> snackbarUtil.showSnackbar(view!!, R.string.generic_request_error)
        }
    }
    private fun setUpMaps() {
        edt_user.setTag(R.id.field_type_key, FormField.Email)
        edt_password.setTag(R.id.field_type_key, FormField.Password)
    }
    @OnClick(R.id.btn_signin)
    fun submitLoginInformation() {
        if (viewModel()!!.validateField(FormInput(FormField.Email, edt_user.text.toString())))
        {
            loadingDialog.show(requireFragmentManager(), null)
            viewModel()!!.submitLoginInformation(edt_user.text.toString(), edt_password.text.toString())
        }
    }

    @OnEditorAction(R.id.edt_password)
    fun onKeyboardAction(textView: TextView, actionId: Int): Boolean {
        return if (actionId == EditorInfo.IME_ACTION_DONE) {
            Keyboard.close(requireActivity(), view!!)
            submitLoginInformation()
            true
        } else {
            false
        }
    }

    @OnClick(R.id.im_back)
    fun navigationBack() {
        navigation.navigateBack()
    }

    override fun onStop() {
        loadingDialog.dismissAllowingStateLoss()
        super.onStop()
    }
}