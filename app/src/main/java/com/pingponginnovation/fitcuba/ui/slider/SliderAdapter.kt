package com.pingponginnovation.fitcuba.ui.slider

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.pingponginnovation.fitcuba.R

class SliderAdapter
internal constructor(fragmentManager: FragmentManager,
                     private val callbackSlider: CallbackSlider) : FragmentStatePagerAdapter(fragmentManager) {

    private val TotalSliders = 3
    private val SliderBeacon = 0
    private val SiderEvent = 1
    private val SliderTourism = 2

    override fun getItem(currentFragment: Int): Fragment {
        var uiSliderInformation: UiSliderInformation? = null
        when(currentFragment){
            SliderBeacon -> uiSliderInformation = UiSliderInformation(R.color.blue_40, R.drawable.slider_beacon, R.string.slider_title_beacon, R.string.slider_description_beacon)
            SiderEvent -> uiSliderInformation = UiSliderInformation(R.color.pink_40, R.drawable.slider_events, R.string.slider_title_event, R.string.slider_description_event)
            SliderTourism -> uiSliderInformation = UiSliderInformation(R.color.green_40, R.drawable.slider_tourism, R.string.slider_title_tourism, R.string.slider_description_tourism)
        }
        return SingleSliderFragment.newInstance(uiSliderInformation!!, callbackSlider)
    }

    override fun getCount() = TotalSliders
}