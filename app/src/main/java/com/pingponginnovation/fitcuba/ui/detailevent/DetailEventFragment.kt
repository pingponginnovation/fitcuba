package com.pingponginnovation.fitcuba.ui.detailevent

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import butterknife.BindView
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.google.android.gms.maps.GoogleMap
import com.metova.slim.annotation.Callback
import com.metova.slim.annotation.Layout
import com.pingponginnovation.fitcuba.R
import com.pingponginnovation.fitcuba.data.NetworkResult
import com.pingponginnovation.fitcuba.module.ApplicationComponentHolder
import com.pingponginnovation.fitcuba.ui.base.ViewModelFragment
import com.pingponginnovation.fitcuba.ui.core.home.UiHome
import com.pingponginnovation.fitcuba.ui.detailevent.innerdetailevent.UiInnerDetailEvent
import com.pingponginnovation.fitcuba.ui.detailevent.innerdetailevent.survey.CallbackSurvey
import com.pingponginnovation.fitcuba.ui.navigation.Navigation
import com.pingponginnovation.fitcuba.ui.navigation.UiAction
import com.pingponginnovation.fitcuba.ui.util.SnackbarUtil
import com.pingponginnovation.fitcuba.ui.util.location.LocationUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_detail_event.*
import javax.inject.Inject

@Layout(R.layout.fragment_detail_event)
class DetailEventFragment : ViewModelFragment<DetailEventViewModel>(), DetailEventCallback, CallbackSurvey {

    companion object {

        private const val Information = "INFORMATION"
        private const val InformationInner = "INFORMATION_INNER"
        private var currentView = 0 // 0 UIHOME - 1 UIINNERDETAILEVENT

        fun newInstance(uiHome: UiHome): DetailEventFragment{
            val fragment = DetailEventFragment()
            val args = Bundle()
            args.putSerializable(Information, uiHome)
            fragment.arguments = args
            this.currentView = 0
            return fragment
        }

        fun newInstance(uiInnerDetailEvent: UiInnerDetailEvent): DetailEventFragment{
            val fragment = DetailEventFragment()
            val args = Bundle()
            args.putSerializable(InformationInner, uiInnerDetailEvent)
            fragment.arguments = args
            this.currentView = 1
            return fragment
        }
    }

    @BindView(R.id.rv_event) lateinit var rvEvent: RecyclerView

    @Inject
    lateinit var snackbarUtil: SnackbarUtil

    @Inject
    lateinit var locationUtil: LocationUtil

    @Callback
    lateinit var navigation: Navigation

    var currentId: Int = 0

    private var adapter: DetailEventAdapter? = null
    private lateinit var detailEvent: UiInnerDetailEvent

    private val TotalColumns = 1

    override fun viewModelClass() = DetailEventViewModel::class.java

    override fun inject() = ApplicationComponentHolder.INSTANCE.getComponent().inject(this)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        verifyPermission()
        setInformation()
    }

    override fun subscribeOnStart() {
        super.subscribeOnStart()

        if(currentView == 0) {
            addSubscription(viewModel()!!.getDetailData(currentId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::setAdapter))
        } else {
            addSubscription(viewModel()!!.getDetailData(detailEvent)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::setAdapter))
            addSubscription(viewModel()!!.getResultSurvey()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleResultSurvey))
        }
    }

    private fun verifyPermission() {
        if (!locationUtil.hasLocationPermission()) {
            locationUtil.requestPermission(requireActivity())
        }
    }

    fun setAdapter(uiEvent: List<UiDetailEventItem>) {
        rvEvent.setHasFixedSize(true)
        rvEvent.layoutManager = GridLayoutManager(context, TotalColumns)
        if(adapter == null) {
            adapter = DetailEventAdapter(context!!, uiEvent, this, this, activity!!.supportFragmentManager)
        }
        rvEvent.adapter = adapter
    }

    private fun handleResultSurvey(networkResult: NetworkResult) {
        when (networkResult) {
            NetworkResult.Success -> snackbarUtil.showSnackbar(view!!, R.string.appreciate_survey)
            NetworkResult.ConnectionError -> snackbarUtil.showSnackbar(view!!, R.string.connection_error)
            else -> snackbarUtil.showSnackbar(view!!, R.string.generic_request_error)
        }
    }

    private fun setInformation() {
        if(currentView == 0) {
            val uiSliderInformation = arguments?.getSerializable(Information) as UiHome
            uiSliderInformation.let {
                Glide.with(context!!).load(it.backgroundImage).into(im_background)
                Glide.with(context!!).load(it.weatherImage).into(img_weather)
                txt_weather.text = it.weather
                txt_title.text = it.title
                txt_description.text = it.description
                currentId = it.idEvent
            }
        } else {
            this.detailEvent = arguments?.getSerializable(InformationInner) as UiInnerDetailEvent
            detailEvent.let {
                Glide.with(context!!).load(it.backgroundImage).into(im_background)
                txt_title.text = it.title
                txt_description.text = it.location
                currentId = it.idEvent
                img_weather.visibility = View.GONE
            }
        }
    }

    override fun goToInnerDetail() {
        navigation.navigateToAction(UiAction.InnerDetail)
    }

    override fun answer(response: Int, idQuestion: Int) {
        viewModel()!!.sendSurvey(response, idQuestion)
    }

    @OnClick(R.id.im_back)
    fun navigationBack() {
        navigation.navigateBack()
    }

    override fun onResume() {
        adapter?.locationHolder?.let {
            it.onResume()
        }
        super.onResume()
    }

    override fun onPause() {
        adapter?.locationHolder?.let {
            it.onPause()
        }
        super.onPause()
    }

    override fun onDestroy() {
        currentView = 0
        adapter?.locationHolder?.onDestroy()
        super.onDestroy()
    }
}