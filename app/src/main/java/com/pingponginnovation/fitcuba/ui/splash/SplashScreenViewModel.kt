package com.pingponginnovation.fitcuba.ui.splash

import android.arch.lifecycle.ViewModel
import com.pingponginnovation.fitcuba.data.login.LoginRepository
import javax.inject.Inject

class SplashScreenViewModel @Inject constructor(private val loginRepository: LoginRepository): ViewModel() {

    init {
        loginRepository.submitAllVisits()
        loginRepository.submitAllSurveys()
    }

    fun getStatusSlider(): Boolean {
        return loginRepository.getStatusSlider()
    }

    fun saveStatusSlider() {
        loginRepository.saveStatusSlider(false)
    }
}