package com.pingponginnovation.fitcuba.persistence.database.events

class PlacesBeacons (
    var idBeacon: Int?,
    var reference: String?,
    var minor: String?,
    var latitudeBeacon: String?,
    var model: String?,
    var photosId: String?,
    var uuid: String?,
    var descriptionsBeacon: List<EventsDescription>?,
    var placesId: Int?,
    var longitudeBeacon: String?,
    var statusBeacon: Int?
)