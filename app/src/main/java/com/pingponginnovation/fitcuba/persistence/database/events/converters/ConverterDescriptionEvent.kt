package com.pingponginnovation.fitcuba.persistence.database.events.converters

import android.arch.persistence.room.TypeConverter
import com.pingponginnovation.fitcuba.persistence.database.events.EventsDescription
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types

class ConverterDescriptionEvent {

    companion object {

        @TypeConverter
        @JvmStatic
        fun toEventDescription(value: String): List<EventsDescription> {
            val moshi = Moshi.Builder().build()
            val type = Types.newParameterizedType(List::class.java, EventsDescription::class.java)
            val adapter = moshi.adapter<List<EventsDescription>>(type)
            return adapter.fromJson(value)!!
        }

        @TypeConverter
        @JvmStatic
        fun fromEventDescription(value: List<EventsDescription>): String {
            val moshi = Moshi.Builder().build()
            val type = Types.newParameterizedType(List::class.java, EventsDescription::class.java)
            val adapter = moshi.adapter<List<EventsDescription>>(type)
            return adapter.toJson(value)
        }
    }
}