package com.pingponginnovation.fitcuba.persistence.database.events

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Single

@Dao
interface EventDao {

    @Query("SELECT * FROM event")
    fun getAllEvents(): Single<List<EventEntity>>

    @Query("SELECT * FROM event WHERE idEvent = :id")
    fun getEvent(id: Int): Single<EventEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(eventEntity: List<EventEntity>)

    @Query("DELETE FROM event")
    fun clearData()
}