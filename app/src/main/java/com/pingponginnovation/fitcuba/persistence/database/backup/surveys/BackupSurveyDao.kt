package com.pingponginnovation.fitcuba.persistence.database.backup.surveys

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Single

@Dao
interface BackupSurveyDao {

    @Query("SELECT * FROM backup_survey")
    fun getAll(): Single<List<BackupSurveyEntity>>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(surveyEntity: BackupSurveyEntity)

    @Query("DELETE FROM backup_survey")
    fun clearData()
}