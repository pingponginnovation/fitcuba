package com.pingponginnovation.fitcuba.persistence.database

import android.support.annotation.VisibleForTesting
import io.reactivex.Scheduler
import io.reactivex.annotations.NonNull
import io.reactivex.functions.Function
import io.reactivex.internal.util.ExceptionHelper
import io.reactivex.schedulers.Schedulers

class Db private constructor() {

    init {
        throw UnsupportedOperationException()
    }

    @VisibleForTesting // Used to change the DB scheduler during tests
    class Plugins private constructor() {

        init {
            throw UnsupportedOperationException()
        }

        companion object {

            fun setSchedulerHandler(@NonNull handler: Function<in Scheduler, out Scheduler>) {
                try {
                    Db.scheduler = handler.apply(DEFAULT_SCHEDULER)
                } catch (exception: Exception) {
                    throw ExceptionHelper.wrapOrThrow(exception)
                }

            }

            fun resetScheduler() {
                scheduler = DEFAULT_SCHEDULER
            }
        }
    }

    companion object {

        const val VERSION = 1
        private val DEFAULT_SCHEDULER = Schedulers.single()
        var scheduler = DEFAULT_SCHEDULER

        @JvmStatic
        fun scheduler(): Scheduler {
            return scheduler
        }
    }
}
