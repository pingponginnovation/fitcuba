package com.pingponginnovation.fitcuba.persistence.database.events.converters

import android.arch.persistence.room.TypeConverter
import com.pingponginnovation.fitcuba.persistence.database.events.PlacesBeacons
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types

class ConverterPlacesBeacons {

    companion object {

        @TypeConverter
        @JvmStatic
        fun toPlacesBeacons(value: String): List<PlacesBeacons> {
            val moshi = Moshi.Builder().build()
            val type = Types.newParameterizedType(List::class.java, PlacesBeacons::class.java)
            val adapter = moshi.adapter<List<PlacesBeacons>>(type)
            return adapter.fromJson(value)!!
        }

        @TypeConverter
        @JvmStatic
        fun fromPlacesBeacons(value: List<PlacesBeacons>): String {
            val moshi = Moshi.Builder().build()
            val type = Types.newParameterizedType(List::class.java, PlacesBeacons::class.java)
            val adapter = moshi.adapter<List<PlacesBeacons>>(type)
            return adapter.toJson(value)
        }
    }
}