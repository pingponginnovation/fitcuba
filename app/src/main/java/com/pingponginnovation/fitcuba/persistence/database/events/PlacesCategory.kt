package com.pingponginnovation.fitcuba.persistence.database.events

class PlacesCategory (
    var idCategory: Int?,
    var updatedAtCategory: String?,
    var nameCategory: String?,
    var createdAtCategory: String?
)