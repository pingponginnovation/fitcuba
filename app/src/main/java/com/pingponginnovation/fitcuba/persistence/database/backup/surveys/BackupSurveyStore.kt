package com.pingponginnovation.fitcuba.persistence.database.backup.surveys

import com.pingponginnovation.fitcuba.data.event.SurveyData
import com.pingponginnovation.fitcuba.persistence.database.Db
import io.reactivex.Single
import javax.inject.Inject

open class BackupSurveyStore @Inject
constructor(protected var surveyDao: BackupSurveyDao) {

    fun getAllData() : Single<List<SurveyData>> {
        return surveyDao.getAll()
            .subscribeOn(Db.scheduler())
            .map { SurveyEntityConversions.toAllSurveysData(it) }
    }
}