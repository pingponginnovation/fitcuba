package com.pingponginnovation.fitcuba.persistence.database.backup.visits

import com.pingponginnovation.fitcuba.data.event.VisitData
import com.pingponginnovation.fitcuba.persistence.database.Db
import io.reactivex.Single
import javax.inject.Inject

open class BackupBeaconStore @Inject
constructor(protected var beaconDao: BackupBeaconDao) {

    fun getAllData() : Single<List<VisitData>> {
        return beaconDao.getAll()
            .subscribeOn(Db.scheduler())
            .map { BeaconEntityConversions.toAllVisitsData(it) }
    }
}