package com.pingponginnovation.fitcuba.persistence.filesystem

import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject

import javax.inject.Inject
import javax.inject.Singleton
import java.io.File
import java.util.HashMap

@Singleton
class FileUpdateSubjects @Inject
constructor()// empty for dagger
{

    private val subjects = HashMap<String, BehaviorSubject<File>>()

    operator fun get(fileNameKey: String): Subject<File>? {
        if (!subjects.containsKey(fileNameKey)) {
            subjects[fileNameKey] = BehaviorSubject.create()
        }
        return subjects[fileNameKey]
    }
}