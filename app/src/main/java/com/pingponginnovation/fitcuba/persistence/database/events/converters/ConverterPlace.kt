package com.pingponginnovation.fitcuba.persistence.database.events.converters

import android.arch.persistence.room.TypeConverter
import com.pingponginnovation.fitcuba.persistence.database.events.Places
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import java.lang.reflect.Type

class ConverterPlace {

    companion object {

        @TypeConverter
        @JvmStatic
        fun toPlace(value: String): List<Places> {
            val moshi = Moshi.Builder().build()
            val type = Types.newParameterizedType(List::class.java, Places::class.java)
            val adapter = moshi.adapter<List<Places>>(type)
            val result = adapter.fromJson(value)
            return result!!
        }

        @TypeConverter
        @JvmStatic
        fun fromPlace(value: List<Places>): String {
            val moshi = Moshi.Builder().build()
            val type:Type = Types.newParameterizedType(List::class.java, Places::class.java)
            val adapter = moshi.adapter<List<Places>>(type)
            return adapter.toJson(value)
        }
    }
}