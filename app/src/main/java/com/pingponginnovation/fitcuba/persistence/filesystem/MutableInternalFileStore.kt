package com.pingponginnovation.fitcuba.persistence.filesystem

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Handler
import android.os.Looper
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.pingponginnovation.fitcuba.module.glide.GlideRequests
import com.pingponginnovation.fitcuba.ui.util.glide.GlideWrapper

import timber.log.Timber

import javax.inject.Inject

class MutableInternalFileStore @Inject
internal constructor(
    context: Context,
    fileUpdateSubjects: FileUpdateSubjects,
    glide: GlideWrapper
) : InternalFileStore(context, fileUpdateSubjects) {

    private val glideRequestManager: GlideRequests = glide.with(context)

    fun saveSplashImage(url: String) {
        saveImageFile(InternalFileStore.FileNames.SPLASH_IMAGE, url)
    }

    private fun saveImageFile(fileName: String, url: String) {
        Handler(Looper.getMainLooper()).post { handleSave(fileName, url) }
    }

    private fun handleSave(fileName: String, url: String) {
        glideRequestManager.asBitmap()
            .load(Uri.parse(url))
            .into(object : SimpleTarget<Bitmap>() {
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    saveImageFile(fileName, resource)
                }

                override fun onLoadFailed(errorDrawable: Drawable?) {
                    Timber.e("Images have failed to load.")
                }
            })
    }
}