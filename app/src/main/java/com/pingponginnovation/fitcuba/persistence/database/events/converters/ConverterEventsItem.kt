package com.pingponginnovation.fitcuba.persistence.database.events.converters

import android.arch.persistence.room.TypeConverter
import com.pingponginnovation.fitcuba.persistence.database.events.EventsItem
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types

class ConverterEventsItem {

    companion object {

        @TypeConverter
        @JvmStatic
        fun toEventsItem(value: String): List<EventsItem> {
            val moshi = Moshi.Builder().build()
            val type = Types.newParameterizedType(List::class.java, EventsItem::class.java)
            val adapter = moshi.adapter<List<EventsItem>>(type)
            return adapter.fromJson(value)!!
        }

        @TypeConverter
        @JvmStatic
        fun fromEventsItem(value: List<EventsItem>): String {
            val moshi = Moshi.Builder().build()
            val type = Types.newParameterizedType(List::class.java, EventsItem::class.java)
            val adapter = moshi.adapter<List<EventsItem>>(type)
            return adapter.toJson(value)
        }
    }
}