package com.pingponginnovation.fitcuba.persistence.database.events

data class EventsDescription(
    var idDescription: Int?,
    var updatedAtDescription: String?,
    var createdAtDescription: String?,
    var textDescription: String?,
    var valueId: Int?,
    var typesId: Int?
)