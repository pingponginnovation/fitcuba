package com.pingponginnovation.fitcuba.persistence.database.events

import com.pingponginnovation.fitcuba.data.event.CityData
import com.pingponginnovation.fitcuba.persistence.database.Db
import io.reactivex.Completable
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject

class MutableEventStore @Inject
constructor(userDao: EventDao) : EventStore(userDao) {

    fun saveEvents(eventsData: List<CityData>) : Completable {
        return Single.fromCallable { EventEntityConversions.toEventEntity(eventsData)}
            .subscribeOn(Db.scheduler())
            .flatMapCompletable { userEntity -> Completable.fromAction { eventDao.insert(userEntity) } }
            .doOnError { error -> Timber.e("Error saving events to database: %s", error.message) }
    }

    fun clearData() {
        Single.fromCallable { }
            .subscribeOn(Db.scheduler)
            .map { eventDao.clearData() }
            .subscribe()
    }
}