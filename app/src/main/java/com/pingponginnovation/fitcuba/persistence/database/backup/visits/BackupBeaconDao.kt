package com.pingponginnovation.fitcuba.persistence.database.backup.visits

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Single

@Dao
interface BackupBeaconDao {

    @Query("SELECT * FROM backup_beacon")
    fun getAll(): Single<List<BackupBeaconEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(beaconEntity: BackupBeaconEntity)

    @Query("DELETE FROM backup_beacon")
    fun clearData()
}