package com.pingponginnovation.fitcuba.persistence.database.events

class PlacesSurveys (
    var idSurvey: Int?,
    var question: String?,
    var updatedAtSurvey: String?,
    var createdAtSurvey: String?,
    var placesId: Int?,
    var order: Int?
)