package com.pingponginnovation.fitcuba.persistence.database.events.converters

import android.arch.persistence.room.TypeConverter
import com.pingponginnovation.fitcuba.persistence.database.events.PlacesTags
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types

class ConverterPlacesTags {

    companion object {

        @TypeConverter
        @JvmStatic
        fun toPlacesTags(value: String): List<PlacesTags> {
            val moshi = Moshi.Builder().build()
            val type = Types.newParameterizedType(List::class.java, PlacesTags::class.java)
            val adapter = moshi.adapter<List<PlacesTags>>(type)
            return adapter.fromJson(value)!!
        }

        @TypeConverter
        @JvmStatic
        fun fromPlacesTags(value: List<PlacesTags>): String {
            val moshi = Moshi.Builder().build()
            val type = Types.newParameterizedType(List::class.java, PlacesTags::class.java)
            val adapter = moshi.adapter<List<PlacesTags>>(type)
            return adapter.toJson(value)
        }
    }
}