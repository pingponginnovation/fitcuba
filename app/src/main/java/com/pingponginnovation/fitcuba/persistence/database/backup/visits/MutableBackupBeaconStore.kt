package com.pingponginnovation.fitcuba.persistence.database.backup.visits

import com.pingponginnovation.fitcuba.data.event.VisitData
import com.pingponginnovation.fitcuba.persistence.database.Db
import io.reactivex.Completable
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject

class MutableBackupBeaconStore @Inject
constructor(beaconDao: BackupBeaconDao) : BackupBeaconStore(beaconDao) {

    fun saveVisit(visitData: VisitData) : Completable {
        return Single.fromCallable { BeaconEntityConversions.toVisitEntity(visitData)}
            .subscribeOn(Db.scheduler())
            .flatMapCompletable { beaconEntity -> Completable.fromAction { beaconDao.insert(beaconEntity) } }
            .doOnError { error -> Timber.e("Error saving events to database: %s", error.message) }
    }

    fun clearData() {
        Single.fromCallable { }
            .subscribeOn(Db.scheduler)
            .map { beaconDao.clearData() }
            .subscribe()
    }
}