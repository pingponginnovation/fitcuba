package com.pingponginnovation.fitcuba.persistence.database.events.converters

import android.arch.persistence.room.TypeConverter
import com.pingponginnovation.fitcuba.persistence.database.events.PlacesSurveys
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types

class ConverterPlacesSurveys {

    companion object {

        @TypeConverter
        @JvmStatic
        fun toPlacesSurveys(value: String): List<PlacesSurveys> {
            val moshi = Moshi.Builder().build()
            val type = Types.newParameterizedType(List::class.java, PlacesSurveys::class.java)
            val adapter = moshi.adapter<List<PlacesSurveys>>(type)
            return adapter.fromJson(value)!!
        }

        @TypeConverter
        @JvmStatic
        fun fromPlacesSurveys(value: List<PlacesSurveys>): String {
            val moshi = Moshi.Builder().build()
            val type = Types.newParameterizedType(List::class.java, PlacesSurveys::class.java)
            val adapter = moshi.adapter<List<PlacesSurveys>>(type)
            return adapter.toJson(value)
        }
    }
}