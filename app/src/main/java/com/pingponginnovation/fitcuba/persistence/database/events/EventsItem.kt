package com.pingponginnovation.fitcuba.persistence.database.events

class EventsItem (
    var idItem: Int?,
    var endDate: String?,
    var startTime: String?,
    var updatedAt: String?,
    var nameItem: String?,
    var endTime: String?,
    var createdAt: String?,
    var descriptionsItem: List<EventsDescription>?,
    var startDate: String?,
    var statusItem: Int?,
    var tags: List<PlacesTags>?,
    val idImage: Int?,
    val idSecondImage: Int?
)