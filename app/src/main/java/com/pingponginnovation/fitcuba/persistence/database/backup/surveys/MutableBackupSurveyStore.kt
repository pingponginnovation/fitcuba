package com.pingponginnovation.fitcuba.persistence.database.backup.surveys

import com.pingponginnovation.fitcuba.data.event.SurveyData
import com.pingponginnovation.fitcuba.persistence.database.Db
import io.reactivex.Completable
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject

class MutableBackupSurveyStore @Inject
constructor(surveyDao: BackupSurveyDao) : BackupSurveyStore(surveyDao) {

    fun saveSurvey(surveyData: SurveyData) : Completable {
        return Single.fromCallable { SurveyEntityConversions.toSurveyEntity(surveyData)}
            .subscribeOn(Db.scheduler())
            .flatMapCompletable { beaconEntity -> Completable.fromAction { surveyDao.insert(beaconEntity) } }
            .doOnError { error -> Timber.e("Error saving events to database: %s", error.message) }
    }

    fun clearData() {
        Single.fromCallable { }
            .subscribeOn(Db.scheduler)
            .map { surveyDao.clearData() }
            .subscribe()
    }
}