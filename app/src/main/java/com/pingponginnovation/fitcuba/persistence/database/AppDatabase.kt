package com.pingponginnovation.fitcuba.persistence.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.pingponginnovation.fitcuba.persistence.database.backup.visits.BackupBeaconEntity
import com.pingponginnovation.fitcuba.persistence.database.backup.visits.BackupBeaconDao
import com.pingponginnovation.fitcuba.persistence.database.backup.surveys.BackupSurveyDao
import com.pingponginnovation.fitcuba.persistence.database.backup.surveys.BackupSurveyEntity
import com.pingponginnovation.fitcuba.persistence.database.events.EventDao
import com.pingponginnovation.fitcuba.persistence.database.events.EventEntity
import com.pingponginnovation.fitcuba.persistence.database.events.converters.*

@Database(
    version = Db.VERSION, entities = [
        EventEntity::class,
        BackupBeaconEntity::class,
        BackupSurveyEntity::class
    ]
)
@TypeConverters(
    ConverterDescriptionEvent::class,
    ConverterEventsItem::class,
    ConverterPlace::class,
    ConverterPlacesBeacons::class,
    ConverterPlacesSurveys::class,
    ConverterPlacesTags::class
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun eventDao(): EventDao

    abstract fun backupBeaconDao(): BackupBeaconDao

    abstract fun backupSurveyDao(): BackupSurveyDao
}