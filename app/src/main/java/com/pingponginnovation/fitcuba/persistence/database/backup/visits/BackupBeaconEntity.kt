package com.pingponginnovation.fitcuba.persistence.database.backup.visits

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "backup_beacon")
data class BackupBeaconEntity(
    @field:PrimaryKey(autoGenerate = true) var idEntity: Int = 0,
    var idBeacon: Int = 0
)