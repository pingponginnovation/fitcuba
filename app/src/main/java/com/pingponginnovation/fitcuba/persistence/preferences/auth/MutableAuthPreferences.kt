package com.pingponginnovation.fitcuba.persistence.preferences.auth

import com.pingponginnovation.fitcuba.persistence.preferences.MutablePreferences
import javax.inject.Inject

class MutableAuthPreferences @Inject
internal constructor(private val mutablePreferences: MutablePreferences) : AuthPreferences(mutablePreferences){

    fun saveUserId(userId: Long) {
        mutablePreferences.save(AuthPreferences.Keys.USER_ID, userId)
    }

    fun saveAuthToken(token: String) {
        mutablePreferences.save(AuthPreferences.Keys.TOKEN, token)
    }

    fun saveStatusSlider(status: Boolean) {
        mutablePreferences.save(AuthPreferences.Keys.SHOW_SLIDER, status)
    }

    fun clearAuthInformation() {
        mutablePreferences.remove(AuthPreferences.Keys.USER_ID)
        mutablePreferences.remove(AuthPreferences.Keys.TOKEN)
    }
}