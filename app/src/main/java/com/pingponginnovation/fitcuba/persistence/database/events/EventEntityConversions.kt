package com.pingponginnovation.fitcuba.persistence.database.events

import com.pingponginnovation.fitcuba.data.event.*
import com.pingponginnovation.fitcuba.ui.detailevent.UiNotification

internal class EventEntityConversions private constructor() {

    init {
        throw UnsupportedOperationException()
    }

    companion object {

        @JvmStatic
        fun toUiNotification(entity: List<EventEntity>, name: String): UiNotification {
            for (event in entity) {
                for(place in event.places) {
                    for(innerBeacon in place.beacons!!) {
                        if(innerBeacon.minor.equals(name)) {
                            return UiNotification(innerBeacon.idBeacon!!, "Fitcuba", innerBeacon.descriptionsBeacon?.get(0)?.textDescription!!, innerBeacon.placesId!!, event.idEvent, 0)
                        }
                    }
                }
            }
            return UiNotification(0, "", "", 0, 0, 0)
        }

        @JvmStatic
        fun toEventData(event: EventEntity): CityData {
            return CityData(event.idEvent, toPlacesData(event.places), event.updatedAtEvent, event.latitude, event.nameEvent, event.createdAtEvent,
                event.slogan, toDescriptionData(event.descriptionsEvent), event.longitude, event.order)
        }

        @JvmStatic
        fun toEventData(entity: List<EventEntity>): List<CityData> {
            val eventData = arrayListOf<CityData>()
            for (event in entity) {
                eventData.add(CityData(event.idEvent, toPlacesData(event.places), event.updatedAtEvent, event.latitude, event.nameEvent, event.createdAtEvent,
                    event.slogan, toDescriptionData(event.descriptionsEvent), event.longitude, event.order))
            }
            return eventData
        }

        @JvmStatic
        private fun toPlacesData(entityPlaces: List<Places>): List<PlacesData> {
            val placesData = arrayListOf<PlacesData>()
            for(innerPlace in entityPlaces) {
                placesData.add(PlacesData(innerPlace.idPlace, innerPlace.address, innerPlace.citiesId, innerPlace.latitudePlace, innerPlace.categoriesId,
                    toPlaceSurveysData(innerPlace.surveys), innerPlace.createdAtPlace, toDescriptionData(innerPlace.descriptionsPlace),
                    innerPlace.url, toPlaceBeaconsData(innerPlace.beacons), toPlaceTagsData(innerPlace.tags), innerPlace.updatedAtPlace, innerPlace.phone,
                    innerPlace.namePlace, innerPlace.openingHours, toPlaceCategoryData(innerPlace.category!!), toEventsItemData(innerPlace.eventsPlace), innerPlace.longitudePlace,
                    innerPlace.statusPlace, innerPlace.idImage, innerPlace.idSecondImage))
            }
            return placesData
        }

        @JvmStatic
        private fun toPlaceSurveysData(entitySurveys: List<PlacesSurveys>?): List<PlacesSurveysData>{
            val surveysData = arrayListOf<PlacesSurveysData>()
            if (entitySurveys != null) {
                for(innerSurvey in entitySurveys) {
                    surveysData.add(PlacesSurveysData(innerSurvey.idSurvey, innerSurvey.question, innerSurvey.updatedAtSurvey, innerSurvey.createdAtSurvey,
                        innerSurvey.placesId, innerSurvey.order))
                }
            }
            return surveysData
        }

        @JvmStatic
        private fun toDescriptionData(entityDescription: List<EventsDescription>?): List<EventsDescriptionData>{
            val descriptionData = arrayListOf<EventsDescriptionData>()
            if (entityDescription != null) {
                for(innerDescription in entityDescription) {
                    descriptionData.add(
                        EventsDescriptionData(innerDescription.idDescription, innerDescription.updatedAtDescription, innerDescription.createdAtDescription,
                            innerDescription.textDescription, innerDescription.valueId, innerDescription.typesId)
                    )
                }
            }
            return descriptionData
        }

        @JvmStatic
        private fun toPlaceBeaconsData(entityBeacons: List<PlacesBeacons>?): List<PlacesBeaconsData>{
            val beaconData = arrayListOf<PlacesBeaconsData>()
            if (entityBeacons != null) {
                for(innerBeacon in entityBeacons) {
                    beaconData.add(PlacesBeaconsData(innerBeacon.idBeacon, innerBeacon.reference, innerBeacon.minor, innerBeacon.latitudeBeacon, innerBeacon.model,
                        innerBeacon.photosId, innerBeacon.uuid, toDescriptionData(innerBeacon.descriptionsBeacon), innerBeacon.placesId, innerBeacon.longitudeBeacon,
                        innerBeacon.statusBeacon))
                }
            }
            return beaconData
        }

        @JvmStatic
        private fun toPlaceTagsData(entityTags: List<PlacesTags>?): List<PlacesTagsData>{
            val tagsData = arrayListOf<PlacesTagsData>()
            if (entityTags != null) {
                for(innerTag in entityTags) {
                    tagsData.add(PlacesTagsData(innerTag.idTag, innerTag.tagValue!!.toLowerCase(), innerTag.updatedAtTag, innerTag.createdAtTag))
                }
            }
            return tagsData
        }

        @JvmStatic
        private fun toPlaceCategoryData(entityCategory: PlacesCategory): PlacesCategoryData{
            return PlacesCategoryData(entityCategory.idCategory, entityCategory.updatedAtCategory, entityCategory.nameCategory, entityCategory.createdAtCategory)
        }

        @JvmStatic
        private fun toEventsItemData(entityItems: List<EventsItem>?): List<EventsItemData>{
            val itemsData = arrayListOf<EventsItemData>()
            if (entityItems != null) {
                for(innerItem in entityItems) {
                    itemsData.add(
                        EventsItemData(innerItem.idItem, innerItem.endDate, innerItem.startTime, innerItem.updatedAt, innerItem.nameItem, innerItem.endTime,
                            innerItem.createdAt, toDescriptionData(innerItem.descriptionsItem), innerItem.startDate, innerItem.statusItem,
                            toPlaceTagsData(innerItem.tags), innerItem.idImage, innerItem.idSecondImage)
                    )
                }
            }
            return itemsData
        }


        @JvmStatic
        fun toEventEntity(cityData: List<CityData>): List<EventEntity> {
            val eventEntity = arrayListOf<EventEntity>()
            for(event in cityData) {
                eventEntity.add(EventEntity(event.idCity, toPlaces(event.places), event.updatedAtEvent, event.latitude, event.nameEvent, event.createdAtEvent,
                    event.slogan, toDescription(event.descriptionsEvent), event.longitude, event.order))
            }
            return eventEntity
        }

        @JvmStatic
        private fun toPlaces(entityPlaces: List<PlacesData>): List<Places> {
            val places = arrayListOf<Places>()
            for(innerPlace in entityPlaces) {
                places.add(Places(innerPlace.idPlace, innerPlace.address, innerPlace.citiesId, innerPlace.latitude, innerPlace.categoriesId,
                    toPlaceSurveys(innerPlace.surveys), innerPlace.createdAtPlace, toDescription(innerPlace.descriptions),
                    innerPlace.url, toPlaceBeacons(innerPlace.beacons), toPlaceTags(innerPlace.tags), innerPlace.updatedAtPlace, innerPlace.phone,
                    innerPlace.namePlace, innerPlace.openingHours, toPlaceCategory(innerPlace.category!!), toEventsItem(innerPlace.eventsPlace), innerPlace.longitude,
                    innerPlace.statusPlace,  innerPlace.idImage, innerPlace.idSecondImage))
            }
            return places
        }

        @JvmStatic
        private fun toPlaceSurveys(entitySurveys: List<PlacesSurveysData>?): List<PlacesSurveys>{
            val surveys = arrayListOf<PlacesSurveys>()
            if (entitySurveys != null) {
                for(innerSurvey in entitySurveys) {
                    surveys.add(PlacesSurveys(innerSurvey.idSurvey, innerSurvey.question, innerSurvey.updatedAtSurvey, innerSurvey.createdAtSurvey,
                            innerSurvey.placesId, innerSurvey.order))
                }
            }
            return surveys
        }

        @JvmStatic
        private fun toDescription(entityDescription: List<EventsDescriptionData>?): List<EventsDescription>{
            val description = arrayListOf<EventsDescription>()
            if (entityDescription != null) {
                for(innerDescription in entityDescription) {
                    description.add(
                        EventsDescription(innerDescription.idDescription, innerDescription.updatedAtDescription, innerDescription.createdAtDescription,
                            innerDescription.textDescription, innerDescription.valueId, innerDescription.typesId)
                    )
                }
            }
            return description
        }

        @JvmStatic
        private fun toPlaceBeacons(entityBeacons: List<PlacesBeaconsData>?): List<PlacesBeacons>{
            val beacon = arrayListOf<PlacesBeacons>()
            if (entityBeacons != null) {
                for(innerBeacon in entityBeacons) {
                    beacon.add(PlacesBeacons(innerBeacon.idBeacon, innerBeacon.reference, innerBeacon.minor, innerBeacon.latitude, innerBeacon.model,
                        innerBeacon.photosId, innerBeacon.uuid, toDescription(innerBeacon.descriptions), innerBeacon.placesId, innerBeacon.longitude,
                        innerBeacon.status))
                }
            }
            return beacon
        }

        @JvmStatic
        private fun toPlaceTags(entityTags: List<PlacesTagsData>?): List<PlacesTags>{
            val tags = arrayListOf<PlacesTags>()
            if (entityTags != null) {
                for(innerTag in entityTags) {
                    tags.add(PlacesTags(innerTag.idTag, innerTag.tagValue, innerTag.updatedAtTag, innerTag.createdAtTag))
                }
            }
            return tags
        }

        @JvmStatic
        private fun toPlaceCategory(entityCategory: PlacesCategoryData): PlacesCategory{
            return PlacesCategory(entityCategory.idCategory, entityCategory.updatedAtCategory, entityCategory.nameCategory, entityCategory.createdAtCategory)
        }

        @JvmStatic
        private fun toEventsItem(entityItems: List<EventsItemData>?): List<EventsItem>{
            val items = arrayListOf<EventsItem>()
            if (entityItems != null) {
                for(innerItem in entityItems) {
                    items.add(
                        EventsItem(innerItem.idItem, innerItem.endDate, innerItem.startTime, innerItem.updatedAt, innerItem.nameItem, innerItem.endTime,
                            innerItem.createdAt, toDescription(innerItem.descriptions), innerItem.startDate, innerItem.status,
                            toPlaceTags(innerItem.tags), innerItem.idImage, innerItem.idSecondImage)
                    )
                }
            }
            return items
        }
    }
}