package com.pingponginnovation.fitcuba.persistence.database.events

import com.pingponginnovation.fitcuba.data.event.CityData
import com.pingponginnovation.fitcuba.persistence.database.Db
import com.pingponginnovation.fitcuba.ui.detailevent.UiNotification
import io.reactivex.Single
import javax.inject.Inject

open class EventStore @Inject
constructor(protected var eventDao: EventDao) {

    fun getAllData() : Single<List<CityData>> {
        return eventDao.getAllEvents()
            .subscribeOn(Db.scheduler())
            .map { EventEntityConversions.toEventData(it) }
    }

    fun getCity(idEvent: Int) : Single<CityData> {
        return eventDao.getEvent(idEvent)
            .subscribeOn(Db.scheduler())
            .map { EventEntityConversions.toEventData(it) }
    }

    fun getDataBeacon(name: String) : Single<UiNotification> {
        return eventDao.getAllEvents()
            .subscribeOn(Db.scheduler())
            .map { EventEntityConversions.toUiNotification(it, name) }
    }
}