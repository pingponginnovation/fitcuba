package com.pingponginnovation.fitcuba.persistence.database.backup.surveys

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "backup_survey")
data class BackupSurveyEntity(
    @field:PrimaryKey(autoGenerate = true) var idEntity: Int = 0,
    var idSurvey: Int = 0,
    var idAnswer: Int = 0
)