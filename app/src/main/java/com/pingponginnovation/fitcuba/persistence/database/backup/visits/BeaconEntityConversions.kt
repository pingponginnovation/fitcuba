package com.pingponginnovation.fitcuba.persistence.database.backup.visits

import com.pingponginnovation.fitcuba.data.event.VisitData

internal class BeaconEntityConversions private constructor() {

    init {
        throw UnsupportedOperationException()
    }

    companion object {

        @JvmStatic
        fun toAllVisitsData(visitEntity: List<BackupBeaconEntity>): List<VisitData> {
            val visitData = arrayListOf<VisitData>()
            for(visit in visitEntity) {
                visitData.add(VisitData(0, visit.idBeacon))
            }
            return visitData
        }

        @JvmStatic
        fun toVisitEntity(visitData: VisitData) = BackupBeaconEntity(idBeacon = visitData.beaconId)
    }
}