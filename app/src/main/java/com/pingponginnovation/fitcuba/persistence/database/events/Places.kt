package com.pingponginnovation.fitcuba.persistence.database.events

class Places(
    var idPlace: Int?,
    var address: String?,
    var citiesId: Int?,
    var latitudePlace: String?,
    var categoriesId: Int?,
    var surveys: List<PlacesSurveys>?,
    var createdAtPlace: String?,
    var descriptionsPlace: List<EventsDescription>?,
    var url: String?,
    var beacons: List<PlacesBeacons>?,
    var tags: List<PlacesTags>?,
    var updatedAtPlace: String?,
    var phone: String?,
    var namePlace: String?,
    var openingHours: String?,
    var category: PlacesCategory?,
    var eventsPlace: List<EventsItem>?,
    var longitudePlace: String?,
    var statusPlace: Int?,
    var idImage: Int?,
    var idSecondImage: Int?
)