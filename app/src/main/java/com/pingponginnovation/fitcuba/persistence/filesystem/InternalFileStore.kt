package com.pingponginnovation.fitcuba.persistence.filesystem

import android.content.Context
import android.graphics.Bitmap
import android.support.annotation.VisibleForTesting
import io.reactivex.Observable
import timber.log.Timber

import javax.inject.Inject
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

open class InternalFileStore @Inject
internal constructor(
    private val context: Context,
    private val fileUpdateSubjects: FileUpdateSubjects
) {

    private val internalFilesDir: File = context.filesDir

    val splashImageFile: Observable<File>?
        get() = getFile(FileNames.SPLASH_IMAGE, FileNames.SPLASH_IMAGE)

    fun getFile(filename: String, alternateFileName: String): Observable<File>? {
        var subject = fileUpdateSubjects[filename]
        val file = File(internalFilesDir.absolutePath, filename)

        val alternateFile = File(internalFilesDir.absolutePath, alternateFileName)

        if (file.exists()) {
            subject!!.onNext(file)
        } else if (alternateFile.exists()) {
            Timber.i("Intended image not found. Returning alternate image.")
            subject = fileUpdateSubjects[alternateFileName]
            subject!!.onNext(alternateFile)
        }
        return subject
    }

    fun saveImageFile(fileName: String, bitmap: Bitmap) {
        val file = File(internalFilesDir.absolutePath, fileName)
        try {
            writeBitmapToFile(bitmap, file)
            fileUpdateSubjects[fileName]!!.onNext(file)
        } catch (e: Exception) {
            Timber.e("Error saving image file")
        }

    }

    @VisibleForTesting
    @Throws(IOException::class)
    private fun writeBitmapToFile(bitmap: Bitmap, imageFile: File) {
        val outputStream = FileOutputStream(imageFile)
        val quality = 100
        bitmap.compress(Bitmap.CompressFormat.PNG, quality, outputStream)
        outputStream.flush()
        outputStream.close()
    }

    internal class FileNames private constructor() {

        init {
            throw UnsupportedOperationException()
        }

        companion object {
            const val SPLASH_IMAGE = "splash_image.png"
        }
    }
}