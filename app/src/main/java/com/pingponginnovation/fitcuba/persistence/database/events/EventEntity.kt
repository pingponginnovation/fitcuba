package com.pingponginnovation.fitcuba.persistence.database.events

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "event")
data class EventEntity(
    @field:PrimaryKey var idEvent: Int = 0,
    var places: List<Places>,
    var updatedAtEvent: String = "",
    var latitude: String = "",
    var nameEvent: String = "",
    var createdAtEvent: String = "",
    var slogan: String = "",
    var descriptionsEvent: List<EventsDescription>,
    var longitude: String = "",
    var order: Int = 0
)