package com.pingponginnovation.fitcuba.persistence.database.backup.surveys

import com.pingponginnovation.fitcuba.data.event.SurveyData

internal class SurveyEntityConversions private constructor() {

    init {
        throw UnsupportedOperationException()
    }

    companion object {

        @JvmStatic
        fun toAllSurveysData(surveyEntity: List<BackupSurveyEntity>): List<SurveyData> {
            val surveyData = arrayListOf<SurveyData>()
            for(survey in surveyEntity) {
                surveyData.add(SurveyData(survey.idAnswer, survey.idSurvey, 0))
            }
            return surveyData
        }

        @JvmStatic
        fun toSurveyEntity(visitData: SurveyData) = BackupSurveyEntity(idSurvey = visitData.surveysId, idAnswer = visitData.answer)
    }
}