package com.pingponginnovation.fitcuba.persistence.database.events

class PlacesTags (
    var idTag: Int?,
    var tagValue: String?,
    var updatedAtTag: String?,
    var createdAtTag: String?
)