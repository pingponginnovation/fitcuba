package com.pingponginnovation.fitcuba

import android.app.Application
import com.google.firebase.messaging.FirebaseMessaging
import com.pingponginnovation.fitcuba.module.ApplicationComponentHolder
import com.pingponginnovation.fitcuba.module.ContextModule
import com.pingponginnovation.fitcuba.module.DaggerApplicationComponent
import com.pingponginnovation.fitcuba.module.NetworkModule

class BaseApplication: Application() {

    companion object {
        private const val  TOPIC = "/topics/fitcuba"
    }

    override fun onCreate() {
        super.onCreate()
        ApplicationComponentHolder.getInstance().setComponent(DaggerApplicationComponent.builder()
                .contextModule(ContextModule(this))
                .networkModule(NetworkModule())
                .build())
        subscribeTopicFirebaseNotification()
    }

    private fun subscribeTopicFirebaseNotification() {
        FirebaseMessaging.getInstance().subscribeToTopic(TOPIC)
    }
}