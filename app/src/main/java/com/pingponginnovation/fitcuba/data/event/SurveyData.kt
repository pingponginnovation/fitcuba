package com.pingponginnovation.fitcuba.data.event

data class SurveyData (val answer: Int,
                       val surveysId: Int,
                       val userId: Int)