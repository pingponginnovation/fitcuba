package com.pingponginnovation.fitcuba.data.event

data class PlacesTagsData (
    val idTag: Int?,
    val tagValue: String?,
    val updatedAtTag: String?,
    val createdAtTag: String?
)