package com.pingponginnovation.fitcuba.data.event

data class VisitData(val userId: Int, val beaconId: Int)