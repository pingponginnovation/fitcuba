package com.pingponginnovation.fitcuba.data.core

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class WeatherCache @Inject
constructor()//For dagger
{
    var weatherTrinidad: Double? = null
    var weatherVinales: Double? = null
}
