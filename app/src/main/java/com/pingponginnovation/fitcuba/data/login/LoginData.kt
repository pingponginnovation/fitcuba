package com.pingponginnovation.fitcuba.data.login

data class LoginData(
    val tokenType: String,
    val expiresIn: Long,
    val accessToken: String,
    val refreshToken: String,
    val userId: Int,
    val type: LoginType
)