package com.pingponginnovation.fitcuba.data.event

data class EventsItemData (
    val idItem: Int?,
    val endDate: String?,
    val startTime: String?,
    val updatedAt: String?,
    val nameItem: String?,
    val endTime: String?,
    val createdAt: String?,
    val descriptions: List<EventsDescriptionData>?,
    val startDate: String?,
    val status: Int?,
    val tags: List<PlacesTagsData>?,
    val idImage: Int?,
    val idSecondImage: Int?
)