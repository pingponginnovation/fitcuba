package com.pingponginnovation.fitcuba.data.event

data class PlacesSurveysData (
    val idSurvey: Int?,
    val question: String?,
    val updatedAtSurvey: String?,
    val createdAtSurvey: String?,
    val placesId: Int?,
    val order: Int?
)