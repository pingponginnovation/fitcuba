package com.pingponginnovation.fitcuba.data.user

data class UserData(
    val id: Long,
    val user: String,
    val password: String
)