package com.pingponginnovation.fitcuba.data

enum class NetworkResult {
    Success,
    AuthorizationError,
    ForbiddenError,
    NotFoundError,
    ConnectionError,
    GenericError
}