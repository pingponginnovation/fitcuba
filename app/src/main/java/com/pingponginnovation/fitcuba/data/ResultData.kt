package com.pingponginnovation.fitcuba.data

class ResultData<T> {

    val networkResult: NetworkResult
    val data: T?

    val isSuccessful: Boolean
        get() = networkResult === NetworkResult.Success

    constructor(data: T) {
        this.data = data
        this.networkResult = NetworkResult.Success
    }

    constructor(networkResult: NetworkResult) {
        if (networkResult === NetworkResult.Success) {
            throw IllegalStateException("Successful result should have accompanying data.")
        }

        this.data = null
        this.networkResult = networkResult
    }
}
