package com.pingponginnovation.fitcuba.data.login

import com.pingponginnovation.fitcuba.data.NetworkResult
import com.pingponginnovation.fitcuba.network.ResponseConversions
import com.pingponginnovation.fitcuba.network.login.LoginResponse
import timber.log.Timber
import javax.inject.Inject

class LoginDataMapper @Inject
internal constructor(private val responseConversions: ResponseConversions) {

    fun toLoginData(loginResponse: LoginResponse) = LoginData(
        loginResponse.token_type,
        loginResponse.expires_in, loginResponse.access_token, loginResponse.refresh_token,
        loginResponse.userId, LoginType.Success
    )

    fun toLoginDataError(throwable: Throwable): LoginData {
        Timber.w("Error submitting login information: %s", throwable.message)
        val type = when (responseConversions.toNetworkResult(throwable)) {
            NetworkResult.ConnectionError -> LoginType.ConnectionError
            NetworkResult.AuthorizationError -> LoginType.InvalidCredentials
            NetworkResult.ForbiddenError -> LoginType.ForbiddenError
            else -> LoginType.GenericError
        }
        return LoginData("", 0, "", "", 0, type)
    }
}