package com.pingponginnovation.fitcuba.data.login

sealed class LoginType {
    object Success : LoginType()
    object InvalidCredentials : LoginType()
    object ForbiddenError : LoginType()
    object ConnectionError : LoginType()
    object GenericError : LoginType()
}