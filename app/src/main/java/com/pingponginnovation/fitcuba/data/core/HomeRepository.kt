package com.pingponginnovation.fitcuba.data.core

import com.pingponginnovation.fitcuba.data.NetworkResult
import com.pingponginnovation.fitcuba.data.ResultData
import com.pingponginnovation.fitcuba.data.event.CityData
import com.pingponginnovation.fitcuba.data.event.SurveyData
import com.pingponginnovation.fitcuba.data.event.VisitData
import com.pingponginnovation.fitcuba.network.core.HomeClient
import com.pingponginnovation.fitcuba.persistence.database.backup.surveys.MutableBackupSurveyStore
import com.pingponginnovation.fitcuba.persistence.database.backup.visits.MutableBackupBeaconStore
import com.pingponginnovation.fitcuba.persistence.database.events.EventStore
import com.pingponginnovation.fitcuba.persistence.database.events.MutableEventStore
import com.pingponginnovation.fitcuba.persistence.preferences.auth.AuthPreferences
import com.pingponginnovation.fitcuba.ui.detailevent.innerdetailevent.UiInnerDetailEvent
import dagger.Reusable
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

@Reusable
class HomeRepository @Inject
internal constructor(
    private val authPreferences: AuthPreferences, private val homeClient: HomeClient,
    private val mutableDatabase: MutableEventStore, private val database: EventStore,
    private val mutableBackupSurveyStore: MutableBackupSurveyStore, private val mutableBackupBeaconStore: MutableBackupBeaconStore,
    private val weatherCache: WeatherCache
) {

    private val eventsDataSubject = PublishSubject.create<ResultData<List<CityData>>>()
    private val actualResult = arrayListOf<UiInnerDetailEvent>()

    fun getIdUser() = authPreferences.getUserId().toInt()

    fun getAllData(): Observable<ResultData<List<CityData>>> {
        getInnerData()
        return eventsDataSubject
    }

    fun getWeather(city: String): Observable<Double> {
        if(city == "Trinidad,cu") {
            return if(weatherCache.weatherTrinidad != null) {
                Observable.just(weatherCache.weatherTrinidad)
            } else {
                homeClient.getWeather(city)
                    .doOnSuccess { weather -> weatherCache.weatherTrinidad = weather }
                    .toObservable()
            }
        } else {
            return if(weatherCache.weatherVinales != null) {
                Observable.just(weatherCache.weatherVinales)
            } else {
                homeClient.getWeather(city)
                    .doOnSuccess { weather -> weatherCache.weatherVinales = weather }
                    .toObservable()
            }
        }
    }

    fun getCity(idCity: Int) = database.getCity(idCity)

    private fun getInnerData() {
        database.getAllData()
            .subscribe { result ->
                if (result.isNotEmpty()) {
                    eventsDataSubject.onNext(ResultData(result))
                } else {
                    requestGetData()
                }
            }
    }

    private fun requestGetData() {
        homeClient.getAllData()
            .doOnSuccess { result ->
                if(result.isSuccessful) {
                    mutableDatabase.saveEvents(result.data!!)
                        .subscribe()
                }
                eventsDataSubject.onNext(result)
            }
            .subscribe()
    }

    fun sendSurvey(answer: Int, surveysId: Int): Completable =
        homeClient.sendSurvey(answer, surveysId, authPreferences.getUserId().toInt())
            .doOnSuccess{result ->
                if(result != NetworkResult.Success) {
                    saveSurveyLocally(answer, surveysId)
                }
             }
            .flatMapCompletable { Completable.complete() }

    fun registerVisitOnServer(idBeacon: Int) {
        homeClient.sendVisit(getIdUser(), idBeacon)
            .doOnSuccess{result ->
                if(result != NetworkResult.Success) {
                    saveVisitLocally(idBeacon)
                }
            }
            .subscribe()
    }

    fun saveActualResultOfInnerPlaces(result: List<UiInnerDetailEvent>) {
        this.actualResult.addAll(result)
    }

    fun makeFilter(search: String): Observable<List<UiInnerDetailEvent>> {
        return if (search.isEmpty()) {
            Observable.just(actualResult)
        } else {
            val filterResult = arrayListOf<UiInnerDetailEvent>()
            for (place in actualResult) {
                for (tag in place.tags) {
                    if (tag.contains(search)) {
                        filterResult.add(place)
                    }
                }
            }
            Observable.just(filterResult)
        }
    }

    private fun saveVisitLocally(idBeacon: Int) {
        mutableBackupBeaconStore.saveVisit(VisitData(0, idBeacon))
            .subscribe()

    }

    private fun saveSurveyLocally(answer: Int, surveysId: Int) {
        mutableBackupSurveyStore.saveSurvey(SurveyData(answer, surveysId, 0))
            .subscribe()
    }
}