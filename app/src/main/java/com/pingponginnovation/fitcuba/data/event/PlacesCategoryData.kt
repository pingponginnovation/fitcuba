package com.pingponginnovation.fitcuba.data.event

data class PlacesCategoryData (
    val idCategory: Int?,
    val updatedAtCategory: String?,
    val nameCategory: String?,
    val createdAtCategory: String?
)