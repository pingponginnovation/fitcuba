package com.pingponginnovation.fitcuba.data.event

data class EventsDescriptionData (
    val idDescription: Int?,
    val updatedAtDescription: String?,
    val createdAtDescription: String?,
    val textDescription: String?,
    val valueId: Int?,
    val typesId: Int?
)