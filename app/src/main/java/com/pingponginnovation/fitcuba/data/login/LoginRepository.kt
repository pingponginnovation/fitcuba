package com.pingponginnovation.fitcuba.data.login

import com.pingponginnovation.fitcuba.data.NetworkResult
import com.pingponginnovation.fitcuba.data.core.HomeRepository
import com.pingponginnovation.fitcuba.network.core.HomeClient
import com.pingponginnovation.fitcuba.network.login.LoginClient
import com.pingponginnovation.fitcuba.persistence.database.backup.surveys.BackupSurveyStore
import com.pingponginnovation.fitcuba.persistence.database.backup.surveys.MutableBackupSurveyStore
import com.pingponginnovation.fitcuba.persistence.database.backup.visits.BackupBeaconStore
import com.pingponginnovation.fitcuba.persistence.database.backup.visits.MutableBackupBeaconStore
import com.pingponginnovation.fitcuba.persistence.database.events.MutableEventStore
import com.pingponginnovation.fitcuba.persistence.preferences.auth.AuthPreferences
import io.reactivex.Single
import javax.inject.Inject
import com.pingponginnovation.fitcuba.persistence.preferences.auth.MutableAuthPreferences

class LoginRepository @Inject
internal constructor(
    private val authPreferences: AuthPreferences, private val mutableAuthPreferences: MutableAuthPreferences,
    private val loginClient: LoginClient, private val mapperData: LoginDataMapper, private val mutableDatabase: MutableEventStore,
    private val mutableBackupSurveyStore: MutableBackupSurveyStore, private val mutableBackupBeaconStore: MutableBackupBeaconStore,
    private val backupSurveyStore: BackupSurveyStore, private val backupBeaconStore: BackupBeaconStore,
    private val homeClient: HomeClient, private val homeRepository: HomeRepository
) {

    fun isSessionActive(): Boolean {
        return authPreferences.getUserId() != 0L
    }

    fun getStatusSlider(): Boolean {
        return authPreferences.getStatusSlider()
    }

    fun getCountries():Single<List<String>>  {
        return loginClient.getCountries()
    }

    fun submitLoginInformation(email: String, password: String): Single<LoginData> {
        return loginClient.submitSignIn(email, password)
            .map(mapperData::toLoginData)
            .doOnSuccess(this::saveUser)
            .onErrorReturn { error -> mapperData.toLoginDataError(error) }
    }

    fun submitNewUser(email: String, password: String, birthday: String, idCountry: Int): Single<LoginData> {
        return loginClient.submitSignUp(email, password, birthday, idCountry)
            .map(mapperData::toLoginData)
            .doOnSuccess(this::saveUser)
            .onErrorReturn { error -> mapperData.toLoginDataError(error) }
    }

    fun submitAllVisits() {
        backupBeaconStore.getAllData()
            .map { allVisits ->
                if(allVisits.isNotEmpty()) {
                    homeClient.sendAllVisits(allVisits, homeRepository.getIdUser())
                        .doOnSuccess { result ->
                            if (result == NetworkResult.Success) {
                                mutableBackupBeaconStore.clearData()
                            }
                        }
                        .subscribe()
                }
            }
            .subscribe()
    }

    fun submitAllSurveys() {
        backupSurveyStore.getAllData()
            .map { allSurveys ->
                if(allSurveys.isNotEmpty()) {
                    homeClient.sendAllSurveys(allSurveys, homeRepository.getIdUser())
                        .doOnSuccess { result ->
                            if (result == NetworkResult.Success) {
                                mutableBackupSurveyStore.clearData()
                            }
                        }
                        .subscribe()
                }
            }
            .subscribe()
    }

    private fun saveUser(loginData: LoginData) {
        mutableAuthPreferences.saveAuthToken(loginData.accessToken)
        mutableAuthPreferences.saveUserId(loginData.userId.toLong())
    }

    fun saveStatusSlider(status: Boolean) {
        mutableAuthPreferences.saveStatusSlider(status)
    }

    fun clearSession() {
        mutableAuthPreferences.clearAuthInformation()
        mutableDatabase.clearData()
        mutableBackupSurveyStore.clearData()
        mutableBackupBeaconStore.clearData()
    }
}