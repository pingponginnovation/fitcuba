package com.pingponginnovation.fitcuba.data.event

data class PlacesBeaconsData (
    val idBeacon: Int?,
    val reference: String?,
    val minor: String?,
    val latitude: String?,
    val model: String?,
    val photosId: String?,
    val uuid: String?,
    val descriptions: List<EventsDescriptionData>?,
    val placesId: Int?,
    val longitude: String?,
    val status: Int?
)