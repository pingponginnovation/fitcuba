package com.pingponginnovation.fitcuba.data.event

data class PlacesData(
    val idPlace: Int?,
    val address: String?,
    val citiesId: Int?,
    val latitude: String?,
    val categoriesId: Int?,
    val surveys: List<PlacesSurveysData>?,
    val createdAtPlace: String?,
    val descriptions: List<EventsDescriptionData>?,
    val url: String?,
    val beacons: List<PlacesBeaconsData>?,
    val tags: List<PlacesTagsData>?,
    val updatedAtPlace: String?,
    val phone: String?,
    val namePlace: String?,
    val openingHours: String?,
    val category: PlacesCategoryData?,
    val eventsPlace: List<EventsItemData>?,
    val longitude: String?,
    val statusPlace: Int?,
    val idImage: Int?,
    val idSecondImage: Int?
)