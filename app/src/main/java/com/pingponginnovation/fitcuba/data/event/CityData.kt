package com.pingponginnovation.fitcuba.data.event

data class CityData (
    val idCity: Int,
    val places: List<PlacesData>,
    val updatedAtEvent: String,
    val latitude: String,
    val nameEvent: String,
    val createdAtEvent: String,
    val slogan: String,
    val descriptionsEvent: List<EventsDescriptionData>,
    val longitude: String,
    val order: Int,
    val idImage: Int = 0
)