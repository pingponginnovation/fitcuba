package com.pingponginnovation.fitcuba.network.events

import com.pingponginnovation.fitcuba.network.adapters.NullToEmptyString

class EventsResponse(
	val places: List<PlacesResponse?>,
	@NullToEmptyString val updated_at: String,
	@NullToEmptyString val latitude: String,
	@NullToEmptyString val name: String,
	@NullToEmptyString val created_at: String,
	val id: Int,
	@NullToEmptyString val slogan: String,
	val descriptions: List<EventsDescriptionsResponse?>,
	@NullToEmptyString val longitude: String,
	val order: Int
)
