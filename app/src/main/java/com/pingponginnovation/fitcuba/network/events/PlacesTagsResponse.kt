package com.pingponginnovation.fitcuba.network.events

import com.pingponginnovation.fitcuba.network.adapters.NullToEmptyString

data class PlacesTagsResponse(
	@NullToEmptyString val tag_value: String? = null,
	@NullToEmptyString val updated_at: String? = null,
	@NullToEmptyString val created_at: String? = null,
	val id: Int? = null
)