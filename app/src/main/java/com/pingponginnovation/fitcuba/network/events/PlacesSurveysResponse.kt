package com.pingponginnovation.fitcuba.network.events

import com.pingponginnovation.fitcuba.network.adapters.NullToEmptyString

data class PlacesSurveysResponse(
	@NullToEmptyString val question: String? = null,
	@NullToEmptyString val updated_at: String? = null,
	@NullToEmptyString val created_at: String? = null,
	val id: Int? = null,
	val places_id: Int? = null,
	val order: Int? = null
)