package com.pingponginnovation.fitcuba.network.events

data class SurveyRequest (private val answer: Int,
                          private val surveys_id: Int,
                          private val users_id: Int)