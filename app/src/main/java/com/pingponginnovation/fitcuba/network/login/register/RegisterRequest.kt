package com.pingponginnovation.fitcuba.network.login.register

data class RegisterRequest(
    val email: String,
    val password: String,
    val birthyear: Int,
    val country: Int
)