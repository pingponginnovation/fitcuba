package com.pingponginnovation.fitcuba.network.events

import com.pingponginnovation.fitcuba.network.adapters.NullToEmptyString

data class EventsItemResponse(
    @NullToEmptyString val end_date: String? = null,
    @NullToEmptyString val start_time: String? = null,
    @NullToEmptyString val updated_at: String? = null,
    @NullToEmptyString val name: String? = null,
    @NullToEmptyString val end_time: String? = null,
    @NullToEmptyString val created_at: String? = null,
    val id: Int? = null,
    val descriptions: List<EventsDescriptionsResponse?>? = null,
    @NullToEmptyString val start_date: String? = null,
    val status: Int? = null,
    val tags: List<PlacesTagsResponse?>? = null,
    val idImage: Int?
)