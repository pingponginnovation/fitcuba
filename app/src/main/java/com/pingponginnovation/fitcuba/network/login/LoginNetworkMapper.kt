package com.pingponginnovation.fitcuba.network.login

import com.pingponginnovation.fitcuba.network.login.register.CountriesResponse
import com.pingponginnovation.fitcuba.network.login.register.RegisterRequest
import com.pingponginnovation.fitcuba.network.login.signin.SignInRequest
import javax.inject.Inject

class LoginNetworkMapper @Inject
internal constructor() { //For dagger

    fun toSignInRequest(email: String, password: String) = SignInRequest(email, password)

    fun toSignUpRequest(email: String, password: String, birthday: String, idCountry: Int) =
        RegisterRequest(email, password, getYear(birthday), idCountry)

    private fun getYear(birthday: String): Int = birthday.substring(0, 4).toInt()

    fun toStringList(countries: List<CountriesResponse>): List<String> {
        val realList = arrayListOf<String>()
        for(country in countries) {
            realList.add(country.name)
        }
        return realList
    }
}