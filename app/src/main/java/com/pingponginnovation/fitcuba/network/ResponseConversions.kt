package com.pingponginnovation.fitcuba.network

import com.pingponginnovation.fitcuba.data.NetworkResult
import retrofit2.HttpException
import javax.inject.Inject
import timber.log.Timber
import java.io.InterruptedIOException
import java.net.SocketException
import java.net.UnknownHostException

class ResponseConversions @Inject
internal constructor() {
    val HttpRequestTimeOut = 408
    val HttpUnauthorized = 401
    val HttpForbidden = 403
    val HttpNotFound = 404

    private fun isConnectionException(throwable: Throwable): Boolean {
        return (throwable is HttpException && isConnectionException(throwable)
                || throwable is UnknownHostException
                || throwable is InterruptedIOException
                || throwable is SocketException)
    }

    private fun isConnectionException(httpException: HttpException): Boolean {
        return httpException.code() === HttpRequestTimeOut
    }

    private fun isAuthorizationException(throwable: Throwable): Boolean {
        return throwable is HttpException && (throwable as HttpException).code() === HttpUnauthorized
    }

    private fun isForbiddenException(throwable: Throwable): Boolean {
        return throwable is HttpException && (throwable as HttpException).code() === HttpForbidden
    }

    private fun isNotFoundException(throwable: Throwable): Boolean {
        return throwable is HttpException && (throwable as HttpException).code() === HttpNotFound
    }

    fun toNetworkResult(throwable: Throwable): NetworkResult {
        Timber.d("Converting to NetworkResult: %s", throwable.message)
        return when {
            isConnectionException(throwable) -> NetworkResult.ConnectionError
            isAuthorizationException(throwable) -> NetworkResult.AuthorizationError
            isForbiddenException(throwable) -> NetworkResult.ForbiddenError
            isNotFoundException(throwable) -> NetworkResult.NotFoundError
            else -> NetworkResult.GenericError
        }
    }
}