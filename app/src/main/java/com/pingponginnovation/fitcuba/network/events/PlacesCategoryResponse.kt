package com.pingponginnovation.fitcuba.network.events

import com.pingponginnovation.fitcuba.network.adapters.NullToEmptyString

data class PlacesCategoryResponse(
	@NullToEmptyString val updated_at: String? = null,
	@NullToEmptyString val name: String? = null,
	@NullToEmptyString val created_at: String? = null,
	val id: Int? = null
)
