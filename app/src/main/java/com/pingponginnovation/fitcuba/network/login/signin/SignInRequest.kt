package com.pingponginnovation.fitcuba.network.login.signin

data class SignInRequest(
    val email: String,
    val password: String
)