package com.pingponginnovation.fitcuba.network.adapters;

import android.support.annotation.Nullable;

import com.squareup.moshi.FromJson;
import com.squareup.moshi.ToJson;

public class NullToEmptyStringAdapter {

  static final String EMPTY_STRING = "";

  @ToJson
  public String toJson(@NullToEmptyString String string) {
    return string;
  }

  @FromJson
  @NullToEmptyString
  public String fromJson(@Nullable String string) {
    return string == null ? EMPTY_STRING : string;
  }
}