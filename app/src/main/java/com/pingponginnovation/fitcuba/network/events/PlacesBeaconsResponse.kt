package com.pingponginnovation.fitcuba.network.events

import com.pingponginnovation.fitcuba.network.adapters.NullToEmptyString

data class PlacesBeaconsResponse(
    @NullToEmptyString val reference: String? = null,
    @NullToEmptyString val minor: String? = null,
    @NullToEmptyString val latitude: String? = null,
    @NullToEmptyString val model: String? = null,
    val id: Int? = null,
    @NullToEmptyString val photos_id: String? = null,
    @NullToEmptyString val uuid: String? = null,
    val descriptions: List<EventsDescriptionsResponse?>? = null,
    val places_id: Int? = null,
    @NullToEmptyString val longitude: String? = null,
    val status: Int? = null
)
