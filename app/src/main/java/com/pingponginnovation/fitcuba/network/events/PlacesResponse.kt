package com.pingponginnovation.fitcuba.network.events

import com.pingponginnovation.fitcuba.network.adapters.NullToEmptyString

class PlacesResponse(
    @NullToEmptyString var address: String = "",
    val cities_id: Int? = null,
    @NullToEmptyString val latitude: String? = null,
    val categories_id: Int? = null,
    val surveys: List<PlacesSurveysResponse?>? = null,
    @NullToEmptyString val created_at: String? = null,
    val descriptions: List<EventsDescriptionsResponse?>? = null,
    @NullToEmptyString val url: String? = null,
    val beacons: List<PlacesBeaconsResponse?>? = null,
    val tags: List<PlacesTagsResponse?>? = null,
    @NullToEmptyString val updated_at: String? = null,
    @NullToEmptyString val phone: String? = null,
    @NullToEmptyString val name: String? = null,
    @NullToEmptyString val opening_hours: String? = null,
    val id: Int? = null,
    val category: PlacesCategoryResponse? = null,
    val events: List<EventsItemResponse?>? = null,
    @NullToEmptyString val longitude: String? = null,
    val status: Int? = null
)
