package com.pingponginnovation.fitcuba.network.login

import com.pingponginnovation.fitcuba.network.NetworkApi
import io.reactivex.Single
import javax.inject.Inject

class LoginClient @Inject
internal constructor(private val api: NetworkApi,
                     private val networkMapper: LoginNetworkMapper) {

    fun submitSignIn(email: String, password: String): Single<LoginResponse> {
        return api.submitSignIn(networkMapper.toSignInRequest(email, password))
    }

    fun submitSignUp(email: String, password: String, birthday: String, idCountry: Int): Single<LoginResponse> {
        return api.submitSignUp(networkMapper.toSignUpRequest(email, password, birthday, idCountry))
    }

    fun getCountries(): Single<List<String>> {
        return api.getCountries()
            .map { result -> networkMapper.toStringList(result) }
    }
}