package com.pingponginnovation.fitcuba.network.events

import com.pingponginnovation.fitcuba.network.adapters.NullToEmptyString

data class EventsDescriptionsResponse(
	@NullToEmptyString val updated_at: String? = null,
	@NullToEmptyString val created_at: String? = null,
	val id: Int? = null,
	@NullToEmptyString val text: String? = null,
	@NullToEmptyString val value_id: Int? = null,
	@NullToEmptyString val types_id: Int? = null
)
