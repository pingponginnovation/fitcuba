package com.pingponginnovation.fitcuba.network.core

data class VisitRequest(val users_id: Int, val beacons_id: Int)