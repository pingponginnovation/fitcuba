package com.pingponginnovation.fitcuba.network.events

data class EventsListResponse (val events: List<EventsResponse>)