package com.pingponginnovation.fitcuba.network.events

data class WeatherResponse(val main: WeatherValueResponse)