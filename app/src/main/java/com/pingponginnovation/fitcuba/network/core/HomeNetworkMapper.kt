package com.pingponginnovation.fitcuba.network.core

import android.content.Context
import com.pingponginnovation.fitcuba.data.ResultData
import com.pingponginnovation.fitcuba.data.event.*
import com.pingponginnovation.fitcuba.network.ResponseConversions
import com.pingponginnovation.fitcuba.network.events.*
import timber.log.Timber
import javax.inject.Inject

class HomeNetworkMapper @Inject
internal constructor(private val responseConversions: ResponseConversions, private val context: Context) {

    fun toSurveyRequest(answer: Int, surveysId: Int, userId: Int): SurveyRequest {
        return SurveyRequest(answer, surveysId, userId)
    }

    fun toAllSurveysRequest(surveyData: List<SurveyData>, userId: Int): List<SurveyRequest> {
        val surveyRequest = arrayListOf<SurveyRequest>()
        for(survey in surveyData) {
            surveyRequest.add(SurveyRequest(survey.answer, survey.surveysId, userId))
        }
        return surveyRequest
    }

    fun toVisitRequest(userId: Int, surveysId: Int): VisitRequest {
        return VisitRequest(userId, surveysId)
    }

    fun toAllVisitsRequest(visitData: List<VisitData>, userId: Int): List<VisitRequest> {
        val visitRequest = arrayListOf<VisitRequest>()
        for(visit in visitData) {
            visitRequest.add(VisitRequest(userId, visit.beaconId))
        }
        return visitRequest
    }

    fun toEventData(response: List<EventsResponse>): ResultData<List<CityData>> {
        val eventData = arrayListOf<CityData>()
        for (event in response) {
            eventData.add(CityData(event.id, toPlacesData(event.places), event.updated_at, event.latitude, event.name, event.created_at,
                event.slogan, toDescriptionData(event.descriptions), event.longitude, event.order))
        }
        return ResultData(eventData)
    }

    fun toEventData(throwable: Throwable): ResultData<List<CityData>> {
        Timber.w("Error getting all information: %s", throwable.message)
        return ResultData(responseConversions.toNetworkResult(throwable))
    }

    private fun toPlacesData(responsePlaces: List<PlacesResponse?>): List<PlacesData> {
        val placesData = arrayListOf<PlacesData>()
        for(innerPlace in responsePlaces) {
            innerPlace?.let {
                placesData.add(PlacesData(it.id, it.address, it.cities_id, it.latitude, it.categories_id,
                    toPlaceSurveysData(it.surveys), it.created_at, toDescriptionData(it.descriptions),
                    it.url, toPlaceBeaconsData(it.beacons), toPlaceTagsData(it.tags), it.updated_at, it.phone,
                    it.name, it.opening_hours, toPlaceCategoryData(it.category!!), toEventsItemData(it.events), it.longitude,
                    it.status, context.resources.getIdentifier("place${it.id}_image1", "drawable", context.packageName),
                    context.resources.getIdentifier("place${it.id}_image2", "drawable", context.packageName)))
            }
        }
        return placesData
    }

    private fun toPlaceSurveysData(responseSurveys: List<PlacesSurveysResponse?>?): List<PlacesSurveysData>{
        val surveysData = arrayListOf<PlacesSurveysData>()
        if (responseSurveys != null) {
            for(innerSurvey in responseSurveys) {
                innerSurvey?.let {
                    surveysData.add(PlacesSurveysData(it.id, it.question, it.updated_at, it.created_at,
                        it.places_id, it.order))
                }
            }
        }
        return surveysData
    }

    private fun toDescriptionData(responseDescription: List<EventsDescriptionsResponse?>?): List<EventsDescriptionData>{
        val descriptionData = arrayListOf<EventsDescriptionData>()
        if (responseDescription != null) {
            for(innerDescription in responseDescription) {
                innerDescription?.let {
                    descriptionData.add(
                        EventsDescriptionData(it.id, it.updated_at, it.created_at,
                            it.text, it.value_id, it.types_id)
                    )
                }
            }
        }
        return descriptionData
    }

    private fun toPlaceBeaconsData(responseBeacons: List<PlacesBeaconsResponse?>?): List<PlacesBeaconsData>{
        val beaconData = arrayListOf<PlacesBeaconsData>()
        if (responseBeacons != null) {
            for(innerBeacon in responseBeacons) {
                innerBeacon?.let {
                    beaconData.add(PlacesBeaconsData(it.id, it.reference, it.minor, it.latitude, it.model,
                        it.photos_id, it.uuid, toDescriptionData(it.descriptions), it.places_id, it.longitude,
                        it.status))
                }
            }
        }
        return beaconData
    }

    private fun toPlaceTagsData(responseTags: List<PlacesTagsResponse?>?): List<PlacesTagsData>{
        val tagsData = arrayListOf<PlacesTagsData>()
        if (responseTags != null) {
            for(innerTag in responseTags) {
                innerTag?.let {
                    tagsData.add(PlacesTagsData(it.id, it.tag_value, it.updated_at, it.created_at))
                }
            }
        }
        return tagsData
    }

    private fun toPlaceCategoryData(responseCategory: PlacesCategoryResponse): PlacesCategoryData{
        return PlacesCategoryData(responseCategory.id, responseCategory.updated_at, responseCategory.name, responseCategory.created_at)
    }

    private fun toEventsItemData(entityItems: List<EventsItemResponse?>?): List<EventsItemData>{
        val itemsData = arrayListOf<EventsItemData>()
        if (entityItems != null) {
            for(innerItem in entityItems) {
                innerItem?.let {
                    itemsData.add(
                        EventsItemData(it.id, it.end_date, it.start_time, it.updated_at, it.name, it.end_time,
                            it.created_at, toDescriptionData(it.descriptions), it.start_date, it.status,
                            toPlaceTagsData(it.tags), context.resources.getIdentifier("event${it.id}_image1", "drawable", context.packageName),
                            context.resources.getIdentifier("event${it.id}_image2", "drawable", context.packageName))
                    )
                }
            }
        }
        return itemsData
        }
}