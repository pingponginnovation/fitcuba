package com.pingponginnovation.fitcuba.network.core

import com.pingponginnovation.fitcuba.data.NetworkResult
import com.pingponginnovation.fitcuba.data.ResultData
import com.pingponginnovation.fitcuba.data.event.CityData
import com.pingponginnovation.fitcuba.data.event.SurveyData
import com.pingponginnovation.fitcuba.data.event.VisitData
import com.pingponginnovation.fitcuba.module.NetworkModule.Companion.WEATHER_API
import com.pingponginnovation.fitcuba.network.NetworkApi
import com.pingponginnovation.fitcuba.network.ResponseConversions
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named

class HomeClient @Inject
internal constructor(
    private val api: NetworkApi,
    @Named(WEATHER_API) private val apiWeather: NetworkApi,
    private val networkMapper: HomeNetworkMapper
) {

    private val ErrorWeather = 0.0
    private val ApiKeyWeather = "f7ada20ab36f9f28db9bf640a8a9d86d"

    fun getAllData(): Single<ResultData<List<CityData>>> {
        return api.getAllData()
            .map(networkMapper::toEventData)
            .onErrorReturn { error -> networkMapper.toEventData(error) }
    }

    fun sendSurvey(answer: Int, surveysId: Int, userId: Int): Single<NetworkResult> {
        return api.sendSurvey(networkMapper.toSurveyRequest(answer, surveysId, userId))
            .toSingleDefault(NetworkResult.Success)
            .onErrorReturn { error -> ResponseConversions().toNetworkResult(error) }
            .doOnError(Timber::e)
    }

    fun sendAllSurveys(surveysData: List<SurveyData>, userId: Int): Single<NetworkResult> {
        return api.sendAllSurvey(networkMapper.toAllSurveysRequest(surveysData, userId))
            .toSingleDefault(NetworkResult.Success)
            .onErrorReturn { error -> ResponseConversions().toNetworkResult(error) }
            .doOnError(Timber::e)
    }

    fun sendVisit(idUser: Int, idBeacon: Int): Single<NetworkResult> {
        return api.sendVisit(networkMapper.toVisitRequest(idUser, idBeacon))
            .toSingleDefault(NetworkResult.Success)
            .onErrorReturn { error -> ResponseConversions().toNetworkResult(error) }
            .doOnError(Timber::e)
    }

    fun sendAllVisits(visitsData: List<VisitData>, userId: Int): Single<NetworkResult> {
        return api.sendAllVisits(networkMapper.toAllVisitsRequest(visitsData, userId))
            .toSingleDefault(NetworkResult.Success)
            .onErrorReturn { error -> ResponseConversions().toNetworkResult(error) }
            .doOnError(Timber::e)
    }

    fun getWeather(city: String): Single<Double> {
        return apiWeather.getWeather(city, ApiKeyWeather)
            .map { weatherResponse -> weatherResponse.main.temp }
            .onErrorReturn { ErrorWeather }
            .doOnError(Timber::e)
    }
}