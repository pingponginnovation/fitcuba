package com.pingponginnovation.fitcuba.network.events

data class WeatherValueResponse(val temp: Double)