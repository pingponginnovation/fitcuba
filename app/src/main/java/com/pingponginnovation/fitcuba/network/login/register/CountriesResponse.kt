package com.pingponginnovation.fitcuba.network.login.register

data class CountriesResponse (val id: Int, val name: String)