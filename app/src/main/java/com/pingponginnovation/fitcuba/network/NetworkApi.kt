package com.pingponginnovation.fitcuba.network

import com.pingponginnovation.fitcuba.network.core.VisitRequest
import com.pingponginnovation.fitcuba.network.events.EventsResponse
import com.pingponginnovation.fitcuba.network.events.SurveyRequest
import com.pingponginnovation.fitcuba.network.events.WeatherResponse
import com.pingponginnovation.fitcuba.network.login.signin.SignInRequest
import com.pingponginnovation.fitcuba.network.login.LoginResponse
import com.pingponginnovation.fitcuba.network.login.register.CountriesResponse
import com.pingponginnovation.fitcuba.network.login.register.RegisterRequest
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface NetworkApi {

    @POST("login")
    fun submitSignIn(@Body signInRequest: SignInRequest): Single<LoginResponse>

    @POST("register")
    fun submitSignUp(@Body registerRequest: RegisterRequest): Single<LoginResponse>

    @POST("survey")
    fun sendSurvey(@Body surveyRequest: SurveyRequest): Completable

    @POST("allSurveys")
    fun sendAllSurvey(@Body surveyRequest: List<SurveyRequest>): Completable

    @POST("visit")
    fun sendVisit(@Body visitRequest: VisitRequest): Completable

    @POST("allVisits")
    fun sendAllVisits(@Body visitRequest: List<VisitRequest>): Completable

    @GET("getAll")
    fun getAllData(): Single<List<EventsResponse>>

    @GET("countries")
    fun getCountries(): Single<List<CountriesResponse>>

    @GET("weather")
    fun getWeather(@Query("q") city: String,
                   @Query("APPID") apiKey: String): Single<WeatherResponse>
}