package com.pingponginnovation.fitcuba.network

import android.content.Context
import android.os.Environment
import com.pingponginnovation.fitcuba.network.core.VisitRequest
import com.pingponginnovation.fitcuba.network.events.EventsListResponse
import com.pingponginnovation.fitcuba.network.events.EventsResponse
import com.pingponginnovation.fitcuba.network.events.SurveyRequest
import com.pingponginnovation.fitcuba.network.events.WeatherResponse
import com.pingponginnovation.fitcuba.network.login.signin.SignInRequest
import com.pingponginnovation.fitcuba.network.login.LoginResponse
import com.pingponginnovation.fitcuba.network.login.register.CountriesResponse
import com.pingponginnovation.fitcuba.network.login.register.RegisterRequest
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import io.reactivex.Completable
import io.reactivex.Single
import okhttp3.MediaType
import okhttp3.ResponseBody
import retrofit2.HttpException
import retrofit2.Response
import timber.log.Timber

import java.io.*
import java.lang.reflect.Type

class CannedNetworkApi(private val context: Context, private val moshi: Moshi): NetworkApi {

    private val mediaTypeJson = MediaType.parse("application/json")
    private val emptyJsonResponse = ResponseBody.create(mediaTypeJson, "{}")

    override fun submitSignIn(signInRequest: SignInRequest): Single<LoginResponse> {
        return when (signInRequest.email) {
            "success@test.com" -> readFileForSingle("cannedData/LoginResponse.json", LoginResponse::class.java)
            "error@test.com" -> Single.error(HttpException(Response.error<ResponseBody>(500, emptyJsonResponse)))
            "connection@test.com" -> Single.error(HttpException(Response.error<ResponseBody>(408, emptyJsonResponse)))
            else -> Single.error(HttpException(Response.error<ResponseBody>(401, emptyJsonResponse)))
        }
    }

    override fun submitSignUp(registerRequest: RegisterRequest): Single<LoginResponse> {
        return when (registerRequest.email) {
            "success@test.com" -> readFileForSingle("cannedData/LoginResponse.json", LoginResponse::class.java)
            "error@test.com" -> Single.error(HttpException(Response.error<ResponseBody>(500, emptyJsonResponse)))
            "connection@test.com" -> Single.error(HttpException(Response.error<ResponseBody>(408, emptyJsonResponse)))
            else -> Single.error(HttpException(Response.error<ResponseBody>(401, emptyJsonResponse)))
        }}

    override fun getAllData(): Single<List<EventsResponse>> {
        return readFileForSingles("cannedData/EventsResponse.json", EventsResponse::class.java)
    }

    override fun getCountries(): Single<List<CountriesResponse>> {
        return readFileForSingles("cannedData/CountriesResponse.json", CountriesResponse::class.java)
    }

    override fun sendVisit(visitRequest: VisitRequest): Completable {
        return Completable.complete()
    }

    override fun sendAllVisits(visitRequest: List<VisitRequest>): Completable {
        return Completable.complete()
    }

    override fun sendSurvey(surveyRequest: SurveyRequest): Completable {
        return Completable.complete()
    }

    override fun sendAllSurvey(surveyRequest: List<SurveyRequest>): Completable {
        return Completable.complete()
    }

    override fun getWeather(city: String, apiKey: String): Single<WeatherResponse> {
        return Single.never()
    }

    private fun <C> readFileForSingle(filePath: String, responseClass: Class<C>): Single<C> {
        val type = Types.newParameterizedType(responseClass)
        return readFileForSingle(filePath, type)
    }

    private fun <C> readFileForSingles(filePath: String, responseClass: Class<C>): Single<List<C>> {
        val type = Types.newParameterizedType(List::class.java, responseClass)
        return readFileForSingle(filePath, type)
    }

    private fun <C> readFileForSingle(filePath: String, type: Type): Single<C> {
        return try {
            val json = readFileForString(filePath)
            val adapter:JsonAdapter<C> = moshi.adapter(type)
            Single.fromCallable{ adapter.fromJson(json)}
        } catch (e: IOException) {
            Single.error(e)
        }
    }

    @Throws(IOException::class)
    private fun readFileForString(filePath: String): String {
        val jsonFile: InputStream = if (isExternalPathValid(filePath)) {
            FileInputStream(File(context.getExternalFilesDir(null), filePath))
        } else {
            context.assets.open(filePath)
        }

        return parseStream(jsonFile)
    }

    private fun isExternalPathValid(relativePath: String): Boolean {
        if (isExternalStorageMounted()) {
            val externalDir = context.getExternalFilesDir(null)
            if (externalDir != null
                    && externalDir.exists()
                    && externalDir.canRead()) {
                val file = File(context.getExternalFilesDir(null), relativePath)
                Timber.i("MOCK - Looking for file: %s", file.absolutePath)
                if (file.exists()) {
                    Timber.i("MOCK - External data found - using instead of in-app mocked data for file: %s", relativePath)
                    return true
                }
            }
        }
        Timber.i("MOCK - No external data found - using in-app mocked data for file: %s", relativePath)
        return false
    }

    @Throws(IOException::class)
    private fun parseStream(stream: InputStream): String {
        return stream.bufferedReader().use(BufferedReader::readText)
    }

    private fun isExternalStorageMounted(): Boolean {
        val state = Environment.getExternalStorageState()
        return Environment.MEDIA_MOUNTED.equals(state)
    }
}
